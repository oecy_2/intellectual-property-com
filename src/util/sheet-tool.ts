export function extractValue(exp = '', obj: Record<string, any> = {}) {
  if (exp.includes('[].')) {
    const key = exp.split('[].')[0];
    const valueExp = exp.split('[].')[1];
    const [name, id] = valueExp.split('&');
    const curObj = obj[key];
    let result = '';
    for (let item of curObj) {
      result += `${item[name]}\\${item[id]}/,`;
    }
    return result.slice(0, result.length - 1);
  } else if (exp.includes('.')) {
    const key = exp.split('.')[0];
    const valueExp = exp.split('.')[1];
    const [name, id] = valueExp.split('&');
    const curObj = obj[key];
    return `${curObj[name]}\\${curObj[id]}/`;
  } else {
    return obj[exp];
  }
}

export const PATENT_SHEET_HEADERS = [
  'id',
  'uuid',
  '专利名称',
  '地址',
  '应用领域',
  '转化方式',
  '转化状态',
  '专利描述',
  '法律状态',
  '应用领域',
  '发明人',
  '技术优势',
];
