import { ENVIRONMENT } from './app-constants';
import { SAP_CONTROL } from '../config/app-config';

/** 域名 */
const _DOMAIN = {
  [ENVIRONMENT.DEV]: 'http://localhost:8000',
  [ENVIRONMENT.TEST]: 'http://localhost:4000',
  [ENVIRONMENT.PRO]: 'http://123.57.194.36:8000',
};

export const DOMAIN = _DOMAIN[SAP_CONTROL];

// 模块
export const PART = {
  OPT_SYS: '/sys',
  OPT_USER: '/user',
  OPT_TALENT_TYPE: '/talentType',
  OPT_GLOBAL: '/global',
  OPT_TALENT: '/talent',
  OPT_PATENT: '/patent',
  OPT_TALENT_STATISTIC: '/talentStatistic',
};

// 返回码
export const RESPONSE_CODE = {
  success: 200,
  created: 201,
  noContent: 204,
  error: 400,
  unauthorized: 401,
  unFind: 404,
  serviceError: 500,
};
