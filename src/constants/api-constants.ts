import * as DominConfigs from "./domin-constants";

/**
 * 系统前端错误日志
 */
export const ERROR_LOG = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_SYS}/errorLog`;

/**
 * 下载模版
 */
export const DOWNLOAD_TEMPLATE = `${DominConfigs.DOMAIN}/downloadTemplate`;
/**
 * 用户列表查询
 */
export const GET_LIST = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}/list`;
export const GET_PRODUCT_FILE = `${DominConfigs.DOMAIN}/getProjectFile`;
export const GET_ALL_PLACE = `${DominConfigs.DOMAIN}/getAllPlace`;
export const GET_STATUS = `${DominConfigs.DOMAIN}/getStatus`;
export const GET_PRODUCT_MAPS = `${DominConfigs.DOMAIN}/getProductMaps`;

export const GET_FINANCIAL_MAP = `${DominConfigs.DOMAIN}/getFinancialMap`;
export const GET_MAPSW = `${DominConfigs.DOMAIN}/getMapsw`;
export const GET_PATENT_MAPS = `${DominConfigs.DOMAIN}/getPatentMaps`;
export const GET_TALENT_MAPS = `${DominConfigs.DOMAIN}/getTalentMaps`;

export const PRODUCT_FILE_EXIST = `${DominConfigs.DOMAIN}/productFileExist`;
export const FINANCIAL_FILE_EXIST = `${DominConfigs.DOMAIN}/financialFileExist`;

export const GET_PRODUCT_LIST = `${DominConfigs.DOMAIN}/getProductList`;
export const UPLOAD_PRODUCT = `${DominConfigs.DOMAIN}/uploadProduct`;
export const CREATE_PRODUCT = `${DominConfigs.DOMAIN}/createProduct`;
export const PRODUCT_UPLOAD_FILES = `${DominConfigs.DOMAIN}/uploadProductFiles`;
export const DELETE_PRODUCT = `${DominConfigs.DOMAIN}/deleteProduct`;
export const GET_USER_LIST = `${DominConfigs.DOMAIN}/getUserList`;
export const DELETE_USER = `${DominConfigs.DOMAIN}/deleteUser`;
export const UPLOAD_USER = `${DominConfigs.DOMAIN}/uploadUser`;
export const CREATE_USER_ITEM = `${DominConfigs.DOMAIN}/createUser`;
export const FREEZE_USER = `${DominConfigs.DOMAIN}/freezeUser`;
export const DELETE_USER_BY_ID = `${DominConfigs.DOMAIN}/deleteUserById`;
export const GET_ORGANIZATION_FILE = `${DominConfigs.DOMAIN}/getOrganizationFile`;
export const UPLOAD_ORGANIZATION_FILE = `${DominConfigs.DOMAIN}/uploadOrganizationFile`;
export const HAS_PRODUCT_EXIST = `${DominConfigs.DOMAIN}/hasProductExist`;
/**
 * 用户增加
 */
export const CREATE_USER = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}`;

/**
 * 用户登录
 */

export const FINANCIAL_LIST = `${DominConfigs.DOMAIN}/financial_list`;
export const UPDATE_FINANCIAL = `${DominConfigs.DOMAIN}/update_financial`;
export const SELECT_LIST = `${DominConfigs.DOMAIN}/selectList`;
export const CREATE_FINANCIAL = `${DominConfigs.DOMAIN}/createFinancial`;
export const DELETE_FINANCIAL = `${DominConfigs.DOMAIN}/deleteFinancial`;
export const UPLOADFINANCIALFILE = `${DominConfigs.DOMAIN}/uploadFinancial`;
export const AUTHORTION = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}/login`;
export const CHECKFINANCIALFILE = `${DominConfigs.DOMAIN}/checkFinancial`;

/**
 * 用户注册
 */
export const REGISTER = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}/register`;
export const TEL_REGISTERED = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}/registered`;

/**
 * 获取选项
 */
export const GET_SELECTIONS = `${DominConfigs.DOMAIN}/global/selections`;
export const GET_ORGANIZATION_SELECTIONS = `${DominConfigs.DOMAIN}/global/organizations`;
export const GET_TALENT_SELECTIONS = `${DominConfigs.DOMAIN}/global/talents`;

/**
 * 专利相关接口
 */
export const GET_PATENTS = `${DominConfigs.DOMAIN}/patent`;
export const DELETE_PATENT = `${DominConfigs.DOMAIN}/patent`;
export const CREATE_PATENT = `${DominConfigs.DOMAIN}/patent`;
export const UPLOAD_PATENTS = `${DominConfigs.DOMAIN}/uploadPatents`;
export const CHECKPATENT = `${DominConfigs.DOMAIN}/checkPatent`;
export const UPDATE_PATENT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_PATENT}/update`;
export const PATENT_FILE_EXIST = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_PATENT}/hasFile`;
/**
 * talentType
 */
export const GET_TALENT_TYPE_LIST = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT_TYPE}/getList`;
export const TALENT_TYPE_INSERT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT_TYPE}/insert`;
export const TALENT_TYPE_DEL = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT_TYPE}/del`;
export const TALENT_TYPE_HAVE_TALENT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT_TYPE}/haveTalent`;

/**
 * 全局信息
 */
export const GLOBAL_SELECTIONS = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_GLOBAL}/selections`;

/**
 * talent
 */
export const TALENT_LIST = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/talentList`;
export const TALENT_INSERT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/insertTalent`;
export const TALENT_DEL_TALENT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/delTalent`;
export const TALENT_UPLOAD_FILES = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/uploadFiles`;
export const TALENT_UPDATE = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/update`;
export const GET_TALENT_ATTACHMENT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/getTalentAttachment`;
export const TALENT_FILE_EXIST = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/hasFile`;
export const TEL_EXIST = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT}/hasSameTel`;

/**
 * user
 */
export const USER_INFO = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}/getInfo`;
export const CHANGE_USER_INFO = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_USER}/changeInfo`;
//联系我们
export const PLATFORM = `${DominConfigs.DOMAIN}/platform`;

/**
 * patent
 */

export const GET_PATENT_ATTACHMENT = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_PATENT}/getPatentAttachment`;
export const AREAPATENTBAR = `${DominConfigs.DOMAIN}/areaBar`;
export const CONVERSIONSTATECHART = `${DominConfigs.DOMAIN}/conversionStateChart`;
export const GETTOTAL = `${DominConfigs.DOMAIN}/getTotal`;
export const GETORGANIAZTIONCHART = `${DominConfigs.DOMAIN}/getOrganizationChart`;
/**
 * talent-statistic
 */
export const ALL_TALENTS = `${DominConfigs.DOMAIN}${DominConfigs.PART.OPT_TALENT_STATISTIC}/allTalents`;
/**
 * tourist
 */
export const TOURIST = `${DominConfigs.DOMAIN}/tourist/tourist`;