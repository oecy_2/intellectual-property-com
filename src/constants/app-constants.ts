/** 环境 */
export const ENVIRONMENT = {
  DEV: 'development', // 开发环境
  TEST: 'test', // 测试环境
  PRO: 'production', // 生产环境
};

/**
 * 主导航
 */
export const GuideMenuType = {
  STATISTIC: '0',
  ACHIEVEMENT: '1',
  TALENT: '2',
  PRODUCT: '3',
  ORGANIZATION: '4',
  COMPANY: '5',
  USER: '6',
  CONTACT_US: '7',
  USER_CENTER: '8',
};

export const LOCAL_STORAGE = 'react-typescript-example';

/**
 * 用户状态
 */
export const USER_STATE = {
  /** 未审核 */
  UNCHECK: 0,
  /** 已通过 */
  PASSED: 1,
  /** 冻结 */
  FREEZE: 2
}

// 用户角色
export const USER_IDENTITY = {
  /** 超机管理员 */
  SUPER_MANAGER: 1,
  /** 普通管理员 */
  MANAGER: 0
}