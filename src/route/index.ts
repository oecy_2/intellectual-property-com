import { RouteConfig } from 'react-router-config';

import Home from 'src/outter-page/home';
import LoginAndRegister from 'src/page/index';
import Platform from 'src/page/platform';
import OrganizationList from 'src/page/organization-list';
import PatentList from 'src/page/patent';
import Unmatch from 'src/page/unmatch';
import TalentList from 'src/page/talent/talent-list';
import ProjectList from 'src/page/product-list';
import UserList from 'src/page/user-list';
import TalentTypeList from 'src/page/talent/talent-classfication';
import UserInfo from 'src/page/user-center/user-info';
import UserChangePassword from 'src/page/user-center/user-change-password';
import AchievementStatistic from 'src/page/achievement-statistic';
import AntdView from 'src/page/antd-view';
import TalentStatistic from 'src/page/talent-statistic';
import ProductStatistic from 'src/page/product-statistic';
import OrganizationStatistic from 'src/page/organization-statistic';
import Company from 'src/page/company';

const config: RouteConfig[] = [
  {
    path: '/',
    component: LoginAndRegister,
    exact: true,
  },
  {
    path: '/home',
    component: Home,
    routes: [
      {
        path: '/home/platform',
        component: Platform,
        exact: true,
      },
      {
        path: '/home/organization-list',
        component: OrganizationList,
        exact: true,
      },
      {
        path: '/home/company',
        component: Company,
        exact: true,
      },
      {
        path: '/home/patent',
        component: PatentList,
        exact: true,
      },
      {
        path: '/home/achievementStatistics',
        component: AchievementStatistic,
        exact: true,
      },
      {
        path: '/home/talentStatistics',
        component: TalentStatistic,
        exact: true,
      },
      {
        path: '/home/antdView',
        component: AntdView,
        exact: true,
      },
      {
        path: '/home/productStatistics',
        component: ProductStatistic,
        exact: true,
      },
      {
        path: '/home/organizationStatistics',
        component: OrganizationStatistic,
        exact: true,
      },
      {
        path: '/home/talent-list',
        component: TalentList,
        exact: true,
      },
      {
        path: '/home/talent-classfication',
        component: TalentTypeList,
        exact: true,
      },
      {
        path: '/home/product-list',
        component: ProjectList,
        exact: true,
      },
      {
        path: '/home/user-list',
        component: UserList,
        exact: true,
      },
      {
        path: '/home/user-info',
        component: UserInfo,
      },
      {
        path: '/home/user-change-password',
        component: UserChangePassword,
      },
      {
        path: '/home/*',
        component: Unmatch,
      },
    ],
  },
  {
    path: '*',
    component: Unmatch,
  },
];

export default config;
