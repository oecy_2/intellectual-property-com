/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Suspense, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { renderRoutes, RouteConfigComponentProps } from "react-router-config";
import {
  Layout,
  Menu,
  Avatar,
  Space,
  Row,
  Col,
  Button,
  Typography,
  Card,
  Divider,
} from "antd";
import { UserOutlined, LogoutOutlined } from "@ant-design/icons";

import "./style.css";
import "antd/dist/antd.css";
import AntdRouterMenu from "../../components/Antd-router-menu";
import PageLoading from "../../components/page-loading";
import useMenu from "./hooks";

const { Content, Footer, Sider, Header } = Layout;
const { Title } = Typography;
export default ({ route }: RouteConfigComponentProps) => {
  const [menu, { setMenuFromGuideType }] = useMenu();
  const history = useHistory();

  /** 是否是游客权限 */
  const isTourist = window.localStorage.getItem("identity") === null;

  /** 显示的用户名 */
  const showUsername = isTourist? "tourist": window.localStorage.getItem("userName");

  /** button显示文字 */

  const buttonText = isTourist? "登录": "退出登录";

  /**
   * 根据主导航切换左侧二级导航
   * @param value
   */
  const changeMenu = (value: any) => {
    console.log("被选中的主菜单栏：", value, value.key, value.item);
    setMenuFromGuideType(value.key);
  };
  useEffect(() => {
    history.push(`${menu ?.[0].url}`);
  }, [menu, history]);

  return (
    <Layout>
      <Sider
        theme='dark'
        className='home-sider'
        breakpoint='lg'
        collapsedWidth='0'
      >
        <div
          style={{
            width: "100wh",
            color: "#ffffff",
            fontSize: "24px",
            padding: "10px 20px",
          }}
        >
          LOGO
        </div>

        <AntdRouterMenu menuData={menu} />
      </Sider>
      <Layout>
        <Header className='header'>
          <Row justify='space-between'>
            <Col xs={2} sm={4} md={6} lg={8} xl={16}>
              <Menu
                theme='dark'
                mode='horizontal'
                defaultSelectedKeys={["1"]}
                onClick={changeMenu}
              >
                <Menu.Item key="0">结果统计</Menu.Item>
                <Menu.Item key="1">成果库</Menu.Item>
                <Menu.Item key="2">人才库</Menu.Item>
                <Menu.Item key="3">产品库</Menu.Item>
                <Menu.Item key="4">金融库</Menu.Item>
                <Menu.Item key="5">企业库</Menu.Item>
                {
                  window.localStorage.getItem('identity') === '1' ?
                    <Menu.Item key="6">用户管理</Menu.Item> : null
                }
                <Menu.Item key="7">联系我们</Menu.Item>
              </Menu>
            </Col>
            <Col xs={20} sm={16} md={12} lg={8} xl={4}>
              <Space>
                <Avatar
                  style={{ backgroundColor: "#87d068" }}
                  icon={
                    <UserOutlined
                      onClick={() => {
                        if(isTourist){
                          return;
                        }
                        changeMenu({ key: "8" });
                      }}
                    />
                  }
                />
                <Button
                  type='text'
                  style={{ fontSize: "16px", color: "#ffffff" }}
                  disabled={isTourist}
                  onClick={() => {
                    changeMenu({ key: "8" });
                  }}
                >   
                  {showUsername}
                </Button>
                <Button
                  type="text"
                  icon={<LogoutOutlined />}
                  style={{ fontSize: "12px", color: "#ffffff" }}
                  onClick={() => {
                    localStorage.clear();
                    history.push("/");
                  }}
                >
                  {buttonText}
                </Button>     
              </Space>
            </Col>
          </Row>
        </Header>

        <div className='home-content-box'>
          <Suspense fallback={<PageLoading />}>
            <Content className='home-content'>
              {renderRoutes(route ?.routes)}
            </Content>
          </Suspense>
        </div>
        <Footer style={{ backgroundColor: "rgb(0, 21, 41)", color: "#ffffff" }}>
          <div>
            <Space align="center">
              <div  >
                <Title level={4} style={{ color: "#ffffff" }}>
                  售前咨询热线
                </Title>

                <Title level={4} style={{ color: "#ffffff" }}>
                  400-888-7654
                </Title>
                <Title level={4} style={{ color: "#ffffff" }}>
                  技术支持
                </Title>
                <Title level={4} style={{ color: "#ffffff" }}>
                  0755 - 8356 7766
                </Title>
              </div>
              <Row justify='space-between' gutter={[30, 0]}>
                <Col>
                  <Card
                    title='相关部门'
                    bordered={false}
                    style={{ backgroundColor: "rgb(0, 21, 41)" }}
                    headStyle={{ color: "#ffffff" }}
                  >
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        科技部
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        工信部
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        卫健委
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        科学技术普及部
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        国务院办公厅
                      </a>
                    </p>
                  </Card>
                </Col>
                <Col>
                  <Card
                    title='直属单位'
                    bordered={false}
                    style={{ backgroundColor: "rgb(0, 21, 41)" }}
                    headStyle={{ color: "#ffffff" }}
                  >
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        创新战略研究院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        学会服务中心
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        企业创新服务中心
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        机关服务中心
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        培训和服务人才中心
                      </a>
                    </p>
                  </Card>
                </Col>
                <Col>
                  <Card
                    title='全国学会'
                    bordered={false}
                    style={{ backgroundColor: "rgb(0, 21, 41)" }}
                    headStyle={{ color: "#ffffff" }}
                  >
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        广东省医疗器械管理学会
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        重庆市医疗器械学会
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        医疗器械专业委员会
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        上海健康医学院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        中国生物医学工程学
                      </a>
                    </p>
                  </Card>
                </Col>
                <Col>
                  <Card
                    title='医疗机构'
                    bordered={false}
                    style={{ backgroundColor: "rgb(0, 21, 41)" }}
                    headStyle={{ color: "#ffffff" }}
                  >
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        北京协和医院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        中国人民解放军总医院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        北京大学第一医院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        瑞金医院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        华西医院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        复旦大学附属中山医院
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        中山大学附属第一医院
                      </a>
                    </p>
                  </Card>
                </Col>
                {/* <Col>
                  <Card
                    title='关于我们'
                    bordered={false}
                    style={{ backgroundColor: "rgb(0, 21, 41)" }}
                    headStyle={{ color: "#ffffff" }}
                  >
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        关于我们
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        联系我们
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        加入我们
                      </a>
                    </p>
                    <p>
                      <a href='#' style={{ color: "rgb(151, 151, 151)" }}>
                        公众号链接：
                      </a>
                    </p>
                    <Image width={200} src='' />
                  </Card>
                </Col> */}
              </Row>
            </Space>
          </div>
          <Divider style={{ backgroundColor: "#ffffff" }} />
          <p style={{ textAlign: "center" }}>code@Eric design@Luna</p>
        </Footer>
      </Layout>
    </Layout>
  );
};
