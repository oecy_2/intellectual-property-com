import { useState } from 'react';

import { GuideMenuType } from '../../constants/app-constants';

import { MenuItem } from '../../components/Antd-router-menu';

const useMenu = (type?: string) => {
  const menuFromGuideType = (type?: string) => {
    switch (type) {
      case GuideMenuType.STATISTIC: {
        return [
          // new MenuItem('/home/achievementStatistics', 'view1'),
          new MenuItem('/home/antdView', '结果统计'),
        ];
      }
      case GuideMenuType.ACHIEVEMENT: {
        return [
          new MenuItem('/home/patent', '专利'),
          new MenuItem('/home/trademark', '著作权'),
        ];
      }
      case GuideMenuType.TALENT: {
        return [
          new MenuItem('/home/talent-list', '人才列表'),
          new MenuItem('/home/talent-classfication', '人才分类'),
        ];
      }
      case GuideMenuType.PRODUCT: {
        return [new MenuItem('/home/product-list', '产品列表')];
      }
      case GuideMenuType.ORGANIZATION: {
        return [new MenuItem('/home/organization-list', '金融机构列表')];
      }
      case GuideMenuType.COMPANY: {
        return [new MenuItem('/home/company', '企业列表')];
      }
      case GuideMenuType.USER: {
        return [
          new MenuItem('/home/user-list', '所有用户'),
          new MenuItem('/home/user-log', '登录日志'),
        ];
      }
      case GuideMenuType.CONTACT_US: {
        return [new MenuItem('/home/platform', '平台信息')];
      }
      case GuideMenuType.USER_CENTER: {
        return [
          new MenuItem('/home/user-info', '个人信息'),
          new MenuItem('/home/user-change-password', '修改密码'),
        ];
      }
      default: {
        return [];
      }
    }
  };
  const [menu, setMenu] = useState<MenuItem[]>([
    new MenuItem('/home/patent', '专利'),
    new MenuItem('/home/trademark', '著作权'),
  ]);

  const setMenuFromGuideType = (currentGuideType?: string) => {
    setMenu(menuFromGuideType(currentGuideType));
  };

  return [menu, { setMenuFromGuideType }] as const;
};

export default useMenu;
