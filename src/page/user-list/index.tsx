import React, { lazy } from 'react';

// @ts-ignore
const GetList = lazy(() => import('./components/getList'));

// 整体组件逻辑,lazy等等.
export default () => <GetList />;
