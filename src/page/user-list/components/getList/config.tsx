import { Button, Divider, Form, Input, Select, Space, Typography } from "antd";
import React from "react";

const { Title } = Typography;

interface configProps {
  setPage: Function;
  setName: Function;
  setDetailShow: Function;
  setRecord: Function;
  getList: Function;
  mainData: any;
  setRole: Function;
  setEmail: Function;
  setStates: Function;
  setPhone: Function;
}

export default (props: configProps) => {
  const {
    setName,
    setPage,
    setDetailShow,
    setRecord,
    mainData,
    setRole,
    setEmail,
    setStates,
    setPhone,
  } = props;

  return (
    <>
      <div>
        <Title level={4}>筛选查询</Title>
      </div>

      <Form layout="inline">
        <Form.Item name="name">
          <Input
            onChange={(e) => {
              setPage(1);
              setName(e.target.value ?? "");
            }}
            placeholder={"请输入用户名字"}
          />
        </Form.Item>
        <Form.Item name="keyword">
          <Input
            onChange={(e) => {
              console.log(e.target.value);
              setPage(1);
              setPhone(e.target.value ?? "");
            }}
            placeholder={"请输入电话"}
          />
        </Form.Item>
        <Form.Item name="application">
          <Input
            onChange={(e) => {
              setPage(1);
              setEmail(e.target.value ?? "");
            }}
            placeholder={"请输入邮箱"}
          />
        </Form.Item>
        <Form.Item
          style={{ width: 200 }}
          label={
            <span style={{ fontWeight: 900, color: "rgb(127,125,142)" }}>
              {"用户状态"}
            </span>
          }
          name="state"
        >
          <Select
            defaultValue={"全部   "}
            onChange={(e) => {
              setPage(1);
              setStates(e ?? "all");
            }}
          >
            <Select.Option key={""} value={"all"}>
              {"全部   "}
            </Select.Option>
            <Select.Option key={"未审核"} value={0}>
              {"未审核"}
            </Select.Option>
            <Select.Option key={"已通过"} value={1}>
              {"已通过"}
            </Select.Option>
            <Select.Option key={"冻结中"} value={2}>
              {"冻结中"}
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          style={{ width: 200 }}
          label={
            <span style={{ fontWeight: 900, color: "rgb(127,125,142)" }}>
              {"用户角色"}
            </span>
          }
          name="date"
        >
          <Select
            defaultValue={"全部   "}
            onChange={(e) => {
              setPage(1);
              setRole(e ?? "");
            }}
          >
            <Select.Option key={""} value={"all"}>
              {"全部   "}
            </Select.Option>
            <Select.Option key={"超级管理员"} value={1}>
              {"超级管理员"}
            </Select.Option>
            <Select.Option key={"普通管理员"} value={0}>
              {"普通管理员"}
            </Select.Option>
          </Select>
        </Form.Item>
      </Form>

      <Divider orientation="left" />
      <h2 style={{ fontWeight: 'bold', display: 'inline-block' }}>详细信息</h2>
      {
        window.localStorage.getItem('identity') === null ?
          null
          :
          <Space style={{ float: 'right' }}>
            <Button
              type="primary"
              onClick={() => {
                if (mainData) {
                  setDetailShow()
                  const organizations = mainData.organizations
                  setRecord({
                    organizations,
                  })
                }
              }}
            >
              添加新管理员
            </Button>
          </Space>
      }
    </>
  );
};
