import {
  Button,
  Cascader,
  Col,
  Form,
  Input,
  message,
  Row,
  Select,
  Space,
} from "antd";
import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  CREATE_USER_ITEM,
  GLOBAL_SELECTIONS,
  UPLOAD_USER,
} from "../../../../constants/api-constants";
import "antd/es/date-picker/style/index";
import md5 from "md5";
import provice from "../../../../util/place";

const { Option } = Select;
const options = provice;

interface detailProps {
  record?: UserList.Item;
  setDetailFalse: Function;
  getList: Function;
}

export default (props: detailProps) => {
  const { record, setDetailFalse, getList } = props;
  const [titleTypeOptions, setTitleTypeOptions] = useState<any>([]);
  const [positionTypeOptions, setPositionTypeOptions] = useState<any>([]);
  useEffect(() => {
    axios.get(GLOBAL_SELECTIONS).then((r) => {
      const { mainData } = r.data;

      const titleOptions = mainData.titleType.map((item: any) => {
        return (
          <Option key={item.uuid} value={item.uuid}>
            {item.name}
          </Option>
        );
      });

      const positionOptions = mainData.positionType.map((item: any) => {
        return (
          <Option key={item.uuid} value={item.uuid}>
            {item.name}
          </Option>
        );
      });

      setTitleTypeOptions(titleOptions);
      setPositionTypeOptions(positionOptions);
    });
  }, []);
  if (!record) {
    return <></>;
  }
  let filters = {};
  const isDetail = record && record.defValue;
  if (isDetail) {
    filters = {
      name: record.name ?? "",
      age: record.age ?? "",
      role: record.role ?? "",
      state: record.state ?? "",
      tel: record.tel ?? "",
      department: record.department ?? "",
      email: record.email ?? "",
      post: record.post ?? "",
      major: record.defValue.major ?? "",
      education: record.defValue.education ?? "",
      sex: record.defValue.sex ?? 0,

      address: record.defValue.address ?? "",
      identity: record.defValue.identity ?? "",
    };
  }
  return (
    <Form
      initialValues={filters}
      onFinish={(e) => {
        console.log(e);
        if (e.password) {
          if (e.password === e.confirm_password) {
            e.password = md5(e.password);
          } else {
            message.error("两次输入密码不一致请重试");
            return;
          }
        }
        if (record.defValue) {
          axios
            .post(UPLOAD_USER, {
              id: record.defValue.id,
              ...e,
              post_uuid: record.defValue.position.uuid,
              role_uuid: record.defValue.title.uuid,
            })
            .then((r) => {
              console.log(r);
              getList();

              setDetailFalse();
            })
            .catch(() => {
              getList();
              setDetailFalse();
            });
        } else {
          axios
            .post(CREATE_USER_ITEM, e)
            .then((r) => {
              console.log(r);
              getList();

              setDetailFalse();
            })
            .catch(() => {
              getList();
              setDetailFalse();
            });
        }
      }}
      layout="horizontal"
      title="机构信息"
    >
      <Row justify="space-between">
        <Col span={4}>
          <Form.Item
            rules={[{ required: true, message: "用户姓名不能为空" }]}
            name="name"
            label="用户姓名"
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[
              {
                pattern: /^1[3456789]\d{9}$/,
                required: true,
                message: "账号不能为空切必须合法",
              },
            ]}
            name="tel"
            label="账号"
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item
            rules={[{ required: !isDetail, message: "密码不能为空" }]}
            name="password"
            label="密码"
          >
            <Input.Password />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[{ required: !isDetail, message: "确认密码不能为空" }]}
            name="confirm_password"
            label="确认密码"
          >
            <Input.Password />
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={4}>
          <Form.Item
            rules={[{ required: true, message: "联系地址不能为空" }]}
            name="address"
            label="联系地址"
          >
            <Cascader options={options} placeholder="请选择地址" />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[
              {
                required: true,
                type: "email",
                message: "邮箱不能为空,且必须合法",
              },
            ]}
            name="email"
            label="邮箱"
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[{ required: true, message: "性别不能为空" }]}
            name="sex"
            label="性别"
          >
            <Select>
              <Option value={1}>男</Option>
              <Option value={0}>女</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[{ required: true, message: "年龄不能为空" }]}
            name="age"
            label="年龄"
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "用户角色不能为空" }]}
            name="identity"
            label="用户角色 "
          >
            <Select>
              <Option value={0}>普通管理员</Option>

              <Option value={1}>超级管理员</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Col>
            <Form.Item
              rules={[{ required: true, message: "学历不能为空" }]}
              label="学历"
              name="education"
            >
              <Select>
                <Option value={"小学"}>小学</Option>
                <Option value={"初中"}>初中</Option>
                <Option value={"高中"}>高中</Option>
                <Option value={"中专"}>中专</Option>
                <Option value={"大专"}>大专</Option>
                <Option value={"大学本科"}>大学本科</Option>
                <Option value={"硕士"}>硕士</Option>
                <Option value={"博士"}>博士</Option>
                <Option value={"博士后"}>博士后</Option>
                <Option value={"院士"}>院士</Option>
              </Select>
            </Form.Item>
          </Col>
        </Col>
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "专业不能为空" }]}
            name="major"
            label="专业"
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={10}>
          <Form.Item
            name="department"
            label="所属机构"
            labelAlign="right"
            rules={[{ message: "所属机构不能为空", required: true }]}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={10}>
          <Form.Item
            label="职务"
            name="post"
            rules={[{ required: true, message: "岗位不能为空" }]}
          >
            <Select>{positionTypeOptions}</Select>
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={10}>
          <Form.Item
            label="职称"
            name="role"
            rules={[{ required: true, message: "职称不能为空" }]}
            labelAlign="right"
          >
            <Select>{titleTypeOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={10}>
          <Form.Item
            rules={[
              {
                message: "用户状态不能为空",
                required: true,
              },
            ]}
            name="state"
            label="用户状态"
            labelAlign="right"
          >
            <Select>
              <Option key={"未审核"} value={0}>
                {"未审核"}
              </Option>
              <Option key={"已通过"} value={1}>
                {"已通过"}
              </Option>
              <Option key={"冻结中"} value={2}>
                {"冻结中"}
              </Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>

      <Form.Item>
        <Space style={{ float: "right" }}>
          <Button type="primary" htmlType="submit">
            提交
          </Button>
          <Button
            key="back"
            onClick={() => {
              setDetailFalse();
            }}
          >
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};
