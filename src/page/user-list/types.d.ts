declare namespace UserList {
  /**
   * 表格数据
   */
  interface Item {
    defValue: any
    key: string
    name: string
    tel: string
    email: string
    age: string
    role: string
    fk_organization: string
    state: string
    post: string
    department: string
    organizations?: any
    orgUUID?: string
    major?: string
  }

  /**
   * antd表格结构
   */
  interface Result {
    total: number
    list: UserList.Item[]
  }
}
