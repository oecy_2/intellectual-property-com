import React, { useEffect, useRef } from "react";
import * as echarts from "echarts";
import "echarts/extension/bmap/bmap";
import area from "../../../../util/area.json";
import axios from "axios";
import { GET_ALL_PLACE } from "../../../../constants/api-constants";

const geoCoordMap: any = area;
const convertData = (data: Array<{ name: string; value: number }>) => {
  let res = [];
  for (var i = 0; i < data.length; i++) {
    const indexString = data[i].name;
    let geoCoord = geoCoordMap[indexString];
    if (geoCoord) {
      res.push({
        name: data[i].name,
        value: geoCoord.concat(data[i].value),
      });
    }
  }
  return res;
};
export default () => {
  let mapRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    let barChart = echarts.init(mapRef.current as HTMLDivElement);
    axios.post(GET_ALL_PLACE).then((r) => {
      console.log(r);
      const { mainData } = r.data;
      let data: any = {};
      let registrationStatuss: any = { 已注册: 0, 未注册: 0 };
      mainData.forEach((v: { address: string; registrationStatus: string }) => {
        const add = v.address?.split(";")[1];
        if (!add) {
          registrationStatuss[v.registrationStatus]++;
          return;
        }
        if (!data.hasOwnProperty(add)) {
          data[add] = 1;
        } else {
          data[add]++;
        }
        registrationStatuss[v.registrationStatus]++;
      });
      console.log(data, registrationStatuss);
      const dataw: any = [];
      for (let name in data) {
        if (data.hasOwnProperty(name)) {
          dataw.push({ name, value: data[name] });
        }
      }
      console.log(dataw);
      const option = {
        title: {
          text: "产品领域分布",
          left: "center",
          textStyle: {
            color: "#235894",
          },
        },
        tooltip: {
          trigger: "item",
        },
        properties: {
          cp: [126.642464, 45.756967],
        },
        bmap: {
          center: [170.642464, 25.756967],
          zoom: 4,
          roam: true,
          mapStyle: {
            styleJson: [
              {
                featureType: "water",
                elementType: "all",
                stylers: {
                  color: "#d1d1d1",
                },
              },
              {
                featureType: "land",
                elementType: "all",
                stylers: {
                  color: "#f3f3f3",
                },
              },
              {
                featureType: "railway",
                elementType: "all",
                stylers: {
                  visibility: "off",
                },
              },
              {
                featureType: "highway",
                elementType: "all",
                stylers: {
                  color: "#fdfdfd",
                },
              },
              {
                featureType: "highway",
                elementType: "labels",
                stylers: {
                  visibility: "off",
                },
              },
              {
                featureType: "arterial",
                elementType: "geometry",
                stylers: {
                  color: "#fefefe",
                },
              },
              {
                featureType: "arterial",
                elementType: "geometry.fill",
                stylers: {
                  color: "#fefefe",
                },
              },
              {
                featureType: "poi",
                elementType: "all",
                stylers: {
                  visibility: "off",
                },
              },
              {
                featureType: "green",
                elementType: "all",
                stylers: {
                  visibility: "off",
                },
              },
              {
                featureType: "subway",
                elementType: "all",
                stylers: {
                  visibility: "off",
                },
              },
              {
                featureType: "manmade",
                elementType: "all",
                stylers: {
                  color: "#d1d1d1",
                },
              },
              {
                featureType: "local",
                elementType: "all",
                stylers: {
                  color: "#d1d1d1",
                },
              },
              {
                featureType: "arterial",
                elementType: "labels",
                stylers: {
                  visibility: "off",
                },
              },
              {
                featureType: "boundary",
                elementType: "all",
                stylers: {
                  color: "#fefefe",
                },
              },
              {
                featureType: "building",
                elementType: "all",
                stylers: {
                  color: "#d1d1d1",
                },
              },
              {
                featureType: "label",
                elementType: "labels.text.fill",
                stylers: {
                  color: "#999999",
                },
              },
            ],
          },
        },
        series: [
          {
            name: "专利领域分布",
            type: "effectScatter",
            coordinateSystem: "bmap",
            data: convertData(
              dataw.sort((a: any, b: any) => {
                return b.value - a.value;
              })
            ),
            symbolSize: function (val: any) {
              return val[2] / 10;
            },
            encode: {
              value: 2,
            },
            showEffectOn: "render",
            rippleEffect: {
              brushType: "stroke",
            },
            hoverAnimation: true,
            label: {
              formatter: "{b}",
              position: "right",
              show: true,
            },

            zlevel: 1,
          },
        ],
      };
      barChart.setOption(option);
    });

    // var bmap = barChart.getModel().getComponent('bmap').getBMap();
    // bmap.addControl(new BMap.MapTypeControl());
  });
  return (
    <>
      <div ref={mapRef} style={{ width: "70vw", height: "80vh" }} />
    </>
  );
};
