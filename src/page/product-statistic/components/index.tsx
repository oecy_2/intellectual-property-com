import { Col, Row } from "antd";
import React from "react";

import Pie from "./chart/pie";
import Bar from "./chart/bar";
import Map from "./chart/map";

export default () => {
  return (
    <>
      <div style={{ display: "flex" }}>
        <Map />
        <div style={{ marginLeft: 30 }}>
          <Row justify="space-between">
            <Col>
              <Bar />
            </Col>
            <Col>
              <Pie />
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
