import React, { lazy } from 'react';

const AntdView = lazy(() => import('./components/index'));

export default () => <AntdView />;