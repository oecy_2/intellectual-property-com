import {Col, Row} from "antd";
import React, {useEffect, useState} from "react";

import MapStatistics from "./layout/map-statistics";
import Patent from "./layout/patent";
import Talent from "./layout/talent";
import Product from "./layout/product";
import Organization from "./layout/organization";
import TotalDetail from "./layout/components/total-detail";
import "./style.css";
import * as APIS from "../../../constants/api-constants";
import axios from "axios";
export default () => {
  const [total, setTotal] = useState({
    patentNum: 0,
    talentNum: 0,
    productNum: 0,
    organizationNum: 0,
  });

  useEffect(() => {
    axios
      .get(APIS.GETTOTAL)
      .then(r => {
        const {mainData, msg} = r.data;
        console.log(msg);
        console.log("maindata:", mainData);
        setTotal(mainData);
      })
      .catch(() => {
        alert("服务器异常");
      });
  }, []);

  return (
    <>
      <Row justify='space-between' gutter={[16, 16]}>
        <Col xs={12} lg={6}>
          <div className='item-box-card'>
            <TotalDetail title='专利总数' value={total.patentNum} />
          </div>
        </Col>
        <Col xs={12} lg={6}>
          <div className='item-box-card'>
            <TotalDetail title='人才总数' value={total.talentNum} />
          </div>
        </Col>
        <Col xs={12} lg={6}>
          <div className='item-box-card'>
            <TotalDetail title='产品总数' value={total.productNum} />
          </div>
        </Col>
        <Col xs={12} lg={6}>
          <div className='item-box-card'>
            <TotalDetail title='机构总数' value={total.organizationNum} />
          </div>
        </Col>
      </Row>
      <Row >
       <Col className='item-box-view1' span={24}>
       <MapStatistics />
       </Col>
      </Row>
      <Row justify='space-between' gutter={16}>
        <Col xs={24} lg={12}>
          <div className='item-box-view1'>
            <Patent /> 
          </div>        
        </Col>
        <Col xs={24} lg={12}> 
          <div className='item-box-view1'>
            <Talent />
          </div>     
        </Col>
      </Row>
      <Row  justify='space-between' gutter={16}>
        <Col xs={24} lg={12}>
          <div className='item-box-view1'>
            <Product />
          </div>  
        </Col>
        <Col xs={24} lg={12}>    
          <div className='item-box-view1'>
            <Organization />
          </div>    
        </Col>
      </Row>
    </>
  );
};
