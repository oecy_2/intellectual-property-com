import React, {useEffect, useState} from "react";
import {Tabs, Typography} from "antd";
import OrganizationTypeBar from "./components/organizationTypeBar";
import axios from "axios";
import OrganizationTypeRadar from "./components/organizationTypeRadar";
import * as APIS from "../../../../constants/api-constants";
const {TabPane} = Tabs;
const {Title} = Typography;

export default () => {
  const [organiazationType, setOrganizationType] = useState<{
    typeName: string[];
    typeResult: number[];
  }>();
  const [organiazationInvest, setOrganizationInvest] = useState<{
    investName: string[];
    investResult: number[];
  }>();
  
  useEffect(() => {
    axios
      .get(APIS.GETORGANIAZTIONCHART)
      .then(r => {
        const {mainData, msg} = r.data;
        console.log(msg);
        console.log("maindata:", mainData);
        setOrganizationType(mainData[0]);
        setOrganizationInvest(mainData[1]);
      })
      .catch(() => {
        alert("服务器异常");
      });
  }, []);
  return (
    <>
      <Tabs tabBarExtraContent={<Title level={5}>机构统计</Title>}>
        <TabPane tab='机构类型' key='1'>
          {organiazationType ? (
            <OrganizationTypeBar
              name={organiazationType?.typeName}
              value={organiazationType?.typeResult}
            ></OrganizationTypeBar>
          ) : (
            <div></div>
          )}
        </TabPane>
        <TabPane tab='投资方向' key='2'>
          {organiazationInvest ? (
            <OrganizationTypeRadar
              name={organiazationInvest?.investName}
              value={organiazationInvest?.investResult}
            ></OrganizationTypeRadar>
          ) : (
            <div></div>
          )}
        </TabPane>
      </Tabs>
    </>
  );
};
