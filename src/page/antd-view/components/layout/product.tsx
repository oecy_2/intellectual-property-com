import React from "react";
import { Tabs, Typography } from "antd";
import ProductPie from "./components/details/product-pie";
import ProductAcross from "./components/details/product-across";

const { TabPane } = Tabs;
const { Title } = Typography;

export default () => {
  return (
    <>
      <Tabs tabBarExtraContent={<Title level={5}>产品统计</Title>}>
        <TabPane tab="注册情况" key="1">
          <ProductPie />
        </TabPane>
        <TabPane tab="应用领域" key="2">
          <ProductAcross />
        </TabPane>
      </Tabs>
    </>
  );
};
