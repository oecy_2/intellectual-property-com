import React, { useEffect, useState } from 'react';
import { Tabs, Typography } from 'antd';
import { Pie, Radar, Column } from '@ant-design/charts';
import { ALL_TALENTS } from '../../../../constants/api-constants';
import axios from 'axios';
const { TabPane } = Tabs;
const { Title } = Typography;

const baseConfig1 = {
  data: [],
  xField: 'name',
  yField: 'star',
  meta: {
    star: {
      alias: '个数',
      min: 0,
      nice: true,
    },
  },
  xAxis: {
    line: null,
    tickLine: null,
  },
  yAxis: {
    label: false,
    grid: {
      alternateColor: 'rgba(0, 0, 0, 0.04)',
    },
  },
  // 开启辅助点
  point: {},
  area: {},
};

const baseConfig2 = {
  appendPadding: 10,
  data: [],
  angleField: 'value',
  colorField: 'type',
  radius: 1,
  innerRadius: 0.6,
  label: {
    type: 'outer',
    content: '{name} {percentage}',
  },
  interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
  statistic: {
    title: false,
    content: {
      style: {
        whiteSpace: 'pre-wrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      },
      content: '行业领域',
    },
  },
};

const baseConfig3 = {
  data: [],
  xField: 'education',
  yField: 'value',
};
export default () => {
  const [config1, setConfig1] = useState(baseConfig1);
  const [config2, setConfig2] = useState(baseConfig2);
  const [config3, setConfig3] = useState(baseConfig3);
  useEffect(() => {
    axios.get(ALL_TALENTS).then(r => {
      const talents = r.data.mainData;
      const types1: any = {};
      const types2: any = {};
      const types3: any = {};
      const data1: any = [];
      const data2: any = [];
      const data3: any = [];

      talents.forEach((item: any) => {
        const type1 = item.talent_classfication.typeName;
        if (types1.hasOwnProperty(type1)) {
          types1[type1] += 1;
        } else {
          types1[type1] = 1;
        }

        const type2 = item.application_area.name;
        if (types2.hasOwnProperty(type2)) {
          types2[type2] += 1;
        } else {
          types2[type2] = 1;
        }

        const type3 = item.education;
        if (types3.hasOwnProperty(type3)) {
          types3[type3] += 1;
        } else {
          types3[type3] = 1;
        }
      });
      const keys1 = Object.keys(types1);
      const keys2 = Object.keys(types2);
      const keys3 = Object.keys(types3);

      for (let key of keys1) {
        data1.push({ name: key, star: types1[key] });
      }
      for (let key of keys2) {
        data2.push({ type: key, value: types2[key] });
      }
      for (let key of keys3) {
        data3.push({ education: key, value: types3[key] });
      }

      setConfig1({ ...baseConfig1, data: data1 });
      setConfig2({ ...baseConfig2, data: data2 });
      setConfig3({ ...baseConfig3, data: data3 });
    });
  }, []);
  return (
    <>
      <Tabs tabBarExtraContent={<Title level={5}>人才统计</Title>}>
        <TabPane tab='人才类型' key='1'>
          <Radar {...config1} />
        </TabPane>
        <TabPane tab='行业领域' key='2'>
          <Pie {...config2} />
        </TabPane>
        <TabPane tab='学历' key='3'>
          <Column {...config3} />
        </TabPane>
      </Tabs>
    </>
  );
};
