import {Card, Statistic} from "antd";
import React from "react";

interface CardDetail {
  title: string;
  value: number;
}
export default ({title, value}: CardDetail) => {
  return (
    <>
      <Card bordered={false}>
        <Statistic
          title={title}
          value={value}
          //precision={2}
          valueStyle={{color: "#3f8600"}}
        />
      </Card>
    </>
  );
};
