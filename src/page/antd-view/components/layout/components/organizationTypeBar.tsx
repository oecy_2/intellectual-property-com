import React, {useEffect, useRef} from "react";
import * as echarts from "echarts";

interface Detail {
  name: string[];
  value: number[];
}
export default ({name, value}: Detail) => {
  const option = {
    xAxis: {
      type: "category",
      data: name,
    },
    yAxis: {
      type: "value",
    },
    series: [
      {
        data: value,
        type: "bar",
      },
    ],
  };
  let barRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    let barChart = echarts.init(barRef.current as HTMLDivElement);
    barChart.setOption(option);
  });
  return (
    <>
      <div ref={barRef} style={{width: "auto", height: 400}}></div>
    </>
  );
};
