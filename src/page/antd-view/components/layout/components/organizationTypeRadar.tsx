import React, {useEffect, useRef} from "react";
import * as echarts from "echarts";

interface Detail {
  name: string[];
  value: number[];
}
export default ({name, value}: Detail) => {
    const max=Math.max(...value);
    const indictor=name.map((v)=>{
return {name:v,max:max}
    })
  const option =  {
    tooltip: {
      trigger: "item",
    },
    title: {
      //  text: '基础雷达图'
    },
    radar: {
        // shape: 'circle',
        indicator: indictor
    },
    series: [{
        //name: '预算 vs 开销（Budget vs spending）',
        type: 'radar',
        data: [
            {
                value: value,
                name: '投资方向'
            },
        
        ]
    }]
};
  let barRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    let barChart = echarts.init(barRef.current as HTMLDivElement);
    barChart.setOption(option);
  });
  return (
    <>
      <div ref={barRef} style={{width: "auto", height: 400}}></div>
    </>
  );
};
