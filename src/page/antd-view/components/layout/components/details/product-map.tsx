import React, { useState } from "react";

import { AMapScene, Popup } from "@antv/l7-react/lib";

import { Spin } from "antd";
import { HeatmapLayer, ILayer, IPickedFeature } from "@antv/l7";
import axios from "axios";
import { GET_PRODUCT_MAPS } from "../../../../../../constants/api-constants";
import { FundTwoTone } from "@ant-design/icons";

const ProductMap: React.FC = () => {
  const [loading, setLoading] = useState(true);
  const [popupInfo, setPopupInfo] = useState<{
    lnglat: number[];
    feature: any;
  }>();
  return (
    <Spin spinning={loading} tip="正在加载地图数据中请稍后...">
      <AMapScene
        onSceneLoaded={(scene: { addLayer: (arg0: ILayer) => void }) => {
          axios.post(GET_PRODUCT_MAPS).then((r) => {
            const { mainData } = r.data;
            const data = mainData.split("\n");
            let sum = 0;
            let num = 0;
            data.forEach(
              (v: { split: (arg0: string) => number[] }, i: number) => {
                if (i !== 0) {
                  const value = v.split(",")[3] ?? 0;
                  if ((value ?? 0) !== 0) {
                    num++;
                  }
                  sum += Number(value);
                }
              }
            );
            const avg = sum / num;
            const layer = new HeatmapLayer({
              onHover(pickedFeature: IPickedFeature): void {
                console.log(pickedFeature);
              },
            })
              .source(mainData, {
                parser: {
                  type: "csv",
                  x: "lng",
                  y: "lat",
                  key: "name",
                },
                transforms: [
                  {
                    type: "hexagon",

                    size: 2500,
                    field: "v",
                    method: "sum",
                  },
                ],
              })
              .size("sum", (sum) => {
                return (sum / avg) * 3000000000;
              })
              .shape("hexagonColumn")
              .style({
                coverage: 40,
                angle: 0,
                opacity: 1.0,
              })
              .color("sum", [
                "#CFF6FF",
                "#2E8AE6",
                "#69D1AB",
                "#DAF291",
                "#FFF5B8",
                "#FFD591",
                "#FF7A45",
                "#105CB3",
                "rgb(98,218,171)",
              ]);
            layer.on("click", function (e) {
              console.log(e.feature);
              setPopupInfo({
                lnglat: e.lngLat,
                feature: e.feature,
              });
            });
            scene.addLayer(layer);
            setLoading(false);
          });
        }}
        map={{
          style: "light",
          pitch: 43,
          center: [107.77791556935472, 32.443286920228644],
          zoom: 5,
        }}
        style={{
          width: "100%",
          height: 600,
          justifyContent: "center",
          position: "relative",
        }}
      >
        {popupInfo && (
          <Popup lnglat={popupInfo.lnglat}>
            <div>
              <h1 style={{ color: "rgb(35,88,148)" }}>{"产品分布  "}</h1>
              <div>
                <FundTwoTone />
                {"    "}
                <span style={{ color: "rgb(28,31,39)" }}>
                  {" "}
                  {popupInfo.feature.rawData[0].name + ":  "}
                </span>
                <span style={{ color: "rgb(63,134,0)" }}>
                  {popupInfo.feature.rawData[0].v}
                </span>
              </div>
            </div>
          </Popup>
        )}
      </AMapScene>
    </Spin>
  );
};

export default ProductMap;
