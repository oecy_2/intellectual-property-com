import React, { useEffect, useState } from "react";
import { Pie } from "@ant-design/charts";
import axios from "axios";
import { GET_ALL_PLACE } from "../../../../../../constants/api-constants";

const ProductPie: React.FC = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    axios.post(GET_ALL_PLACE).then((r) => {
      const { mainData } = r.data;
      setData(mainData);
    });
  }, []);
  const config = {
    appendPadding: 10,
    data: data,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "outer",
      content: "{name} {percentage}",
    },
    interactions: [{ type: "pie-legend-active" }, { type: "element-active" }],
  };

  return <Pie {...config} />;
};

export default ProductPie;
