import React, { useEffect, useState } from "react";
import { Pie } from "@ant-design/charts";
import axios from "axios";
import { GET_STATUS } from "../../../../../../constants/api-constants";

const ProductAcross: React.FC = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    axios.post(GET_STATUS).then((r) => {
      const { mainData } = r.data;
      const { dataAxis, data } = mainData;
      const datas = dataAxis.map((v: any, i: string | number) => {
        return { type: v, value: data[i].value };
      });
      setData(datas);
    });
  }, []);
  const config = {
    appendPadding: 10,
    data: data,
    angleField: "value",
    colorField: "type",
    radius: 1,
    innerRadius: 0.6,
    label: {
      type: "outer",
      content: '{name} {percentage}'
      /* offset: "-50%",
      content: "{value}",
      style: {
        textAlign: "center",
        fontSize: 14,
      }, */
    },
    annotations: [
      {
        type: "image",
        /* src:
          "https://gw.alipayobjects.com/mdn/rms_2274c3/afts/img/A*ELYbTIVCgPoAAAAAAAAAAABkARQnAQ", */
        /** 位置 */
        position: ["50%", "50%"],
        /** 图形样式属性 */
        style: {
          width: 50,
          height: 50,
        },
        /** x 方向的偏移量 */
        offsetX: -25,
        /** y 方向的偏移量 */
        offsetY: 15,
      },
    ],
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: "pre-wrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          color: "#235894",
        },
        content: "应用领域",
      },
    },
  };
  // @ts-ignore
  return <Pie {...config} />;
};

export default ProductAcross;
