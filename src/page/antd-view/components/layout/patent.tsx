import React, { useState, useEffect } from "react";
import { Tabs, Typography } from "antd";
import { Pie, Radar } from "@ant-design/charts";
import { GET_PATENTS } from "../../../../constants/api-constants";
import axios from "axios";

const { TabPane } = Tabs;
const { Title } = Typography;

const baseConfig1 = {
  appendPadding: 10,
  data: [],
  angleField: "value",
  colorField: "type",
  radius: 1,
  innerRadius: 0.6,
  label: {
    type: "outer",
    content: "{name} {percentage}",
  },
  interactions: [{ type: "element-selected" }, { type: "element-active" }],
  statistic: {
    title: false,
    content: {
      style: {
        whiteSpace: "pre-wrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
      },
      content: "应用领域",
    },
  },
};

const baseConfig2 = {
  data: [],
  xField: "name",
  yField: "star",
  meta: {
    star: {
      alias: "个数",
      min: 0,
      nice: true,
    },
  },
  xAxis: {
    line: null,
    tickLine: null,
  },
  yAxis: {
    label: false,
    grid: {
      alternateColor: "rgba(0, 0, 0, 0.04)",
    },
  },
  // 开启辅助点
  point: {},
  area: {},
};

const baseConfig3 = {
  appendPadding: 10,
  data: [],
  angleField: "value",
  colorField: "type",
  radius: 0.8,
  label: {
    type: "outer",
    content: "{name} {percentage}",
  },
  interactions: [{ type: "pie-legend-active" }, { type: "element-active" }],
};

export default () => {
  const [config1, setConfig1] = useState(baseConfig1);
  const [config2, setConfig2] = useState(baseConfig2);
  const [config3, setConfig3] = useState(baseConfig3);
  useEffect(() => {
    axios.get(GET_PATENTS).then((r) => {
      const patents = r.data.mainData.patents;
      const types1: any = {};
      const types2: any = {};
      const types3: any = {};
      const data1: any = [];
      const data2: any = [];
      const data3: any = [];
      console.log("data2:", data2);
      patents.forEach((item: any) => {
        const type1 = item.applicationAreaName;
        if (types1.hasOwnProperty(type1)) {
          types1[type1] += 1;
        } else {
          types1[type1] = 1;
        }

        const type2 = item.type;
        if (types2.hasOwnProperty(type2)) {
          types2[type2] += 1;
        } else {
          types2[type2] = 1;
        }

        const type3 = item.conversionType;
        if (types3.hasOwnProperty(type3)) {
          types3[type3] += 1;
        } else {
          types3[type3] = 1;
        }
      });
      const keys1 = Object.keys(types1);
      const keys2 = Object.keys(types2);
      const keys3 = Object.keys(types3);

      for (let key of keys1) {
        data1.push({ type: key, value: types1[key] });
      }
      for (let key of keys2) {
        data2.push({ name: key, star: types2[key] });
      }
      for (let key of keys3) {
        data3.push({ type: key, value: types3[key] });
      }

      setConfig1({ ...baseConfig1, data: data1 });
      setConfig2({ ...baseConfig2, data: data2 });
      setConfig3({ ...baseConfig3, data: data3 });
    });
  }, []);
  return (
    <>
      <Tabs
        destroyInactiveTabPane={false}
        tabBarExtraContent={<Title level={5}>专利统计</Title>}
      >
        <TabPane tab="应用领域" key="1">
          <Pie {...config1} />
        </TabPane>
        <TabPane tab="专利类型" key="2">
          <Radar {...config2} />
        </TabPane>
        <TabPane tab="转化状态" key="3">
          <Pie {...config3} />
        </TabPane>
      </Tabs>
    </>
  );
};
