import { Tabs, Typography } from "antd";
import React from "react";
import FinalMap from "./components/details/final-map";
import ProductMap from "./components/details/product-map";
import PatentMap from "./components/details/patent-map";
import TalentMap from "./components/details/talent-map";

const { TabPane } = Tabs;
const { Title } = Typography;

export default () => {
  return (
    <>
      <Tabs tabBarExtraContent={<Title level={5}>地域分布</Title>}>
        <TabPane tab="专利" key="1">
          <PatentMap />
        </TabPane>
        <TabPane tab="人才" key="2">
          <TalentMap />
        </TabPane>
        <TabPane tab="产品" key="3">
          <ProductMap />
        </TabPane>
        <TabPane tab="金融机构" key="4">
          <FinalMap />
        </TabPane>
      </Tabs>
    </>
  );
};
