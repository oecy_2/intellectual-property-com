import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import Map from "./chart/map";
import AreaBar from "./chart/areaBar";
import ConversionStateChart from "./chart/conversionStateChart";
import BarProduct from "./chart/barProduct";
import PieProduct from "./chart/pieProduct";
import TalentAreaDistribution from "../../talent-statistic/components/chart/application-distribution";
import TalentTypeDistribution from "../../talent-statistic/components/chart/type-distribution";
import "./style.css";
import * as APIS from "../../../constants/api-constants";
import axios from "axios";
export default () => {
  const [patentAreaBarName, setPatentAreaBarName] = useState([""]);
  const [patentAreaBarNumber, setPatentAreaBarNumber] = useState<number[]>([]);
  const [conversionStateChart, setConversionStateChart] = useState<
    { value: number; name: string }[]
  >();
  useEffect(() => {
    axios
      .get(APIS.AREAPATENTBAR)
      .then((r) => {
        const { mainData, msg } = r.data;
        console.log(msg);
        console.log("maindata:", mainData);
        setPatentAreaBarName(mainData.name);
        setPatentAreaBarNumber(mainData.number);
      })
      .catch(() => {
        alert("服务器异常");
      });
  }, []);
  useEffect(() => {
    axios
      .get(APIS.CONVERSIONSTATECHART)
      .then((r) => {
        const { mainData, msg } = r.data;
        console.log(msg);
        console.log("maindata:", mainData);
        setConversionStateChart(mainData);
      })
      .catch(() => {
        alert("服务器异常");
      });
  }, []);
  return (
    <>
      <div className="statistic">
        <div className="talent">
          {patentAreaBarName && patentAreaBarNumber.length !== 0 ? (
            <div className="item-box">
              <AreaBar name={patentAreaBarName} number={patentAreaBarNumber} />
            </div>
          ) : (
            <div></div>
          )}
          {conversionStateChart ? (
            <div className="item-box">
              <ConversionStateChart
                conversionStateChart={conversionStateChart}
              />
            </div>
          ) : (
            <div></div>
          )}
        </div>
        <div className="organization-patent">
          <Row justify="space-around">
            <Col>
              <div className="item-box">
                <TalentAreaDistribution width="250px" height="300px" />
              </div>
            </Col>
            <Col>
              <div className="item-box">
                <TalentTypeDistribution width="250px" height="300px" />
              </div>
            </Col>
          </Row>
          <Row justify="center">
            <div className="item-box">
              <Map />
            </div>
          </Row>
        </div>
        <div className="product">
          <div className="item-box">
            <BarProduct />
          </div>
          <div className="item-box">
            <PieProduct />
          </div>
        </div>
      </div>
    </>
  );
};
