import React, { useEffect, useRef } from "react";
import * as echarts from "echarts";
import axios from "axios";
import { GET_STATUS } from "../../../../constants/api-constants";

const zoomSize = 6;

export default () => {
  let barRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    let barChart = echarts.init(barRef.current as HTMLDivElement);
    axios.post(GET_STATUS).then((r) => {
      console.log(r);
      const { mainData } = r.data;
      const { dataAxis, data } = mainData;
      const option = {
        title: {
          text: "产品——应用领域",
          textStyle: {
            color: "#235894",
          },
        },
        xAxis: {
          data: dataAxis,
          axisLabel: {
            inside: true,
            textStyle: {
              color: "#fff",
            },
          },
          axisTick: {
            show: false,
          },
          axisLine: {
            show: false,
          },
          z: 10,
        },
        yAxis: {
          axisLine: {
            show: false,
          },
          axisTick: {
            show: false,
          },
          axisLabel: {
            textStyle: {
              color: "#999",
            },
          },
        },
        dataZoom: [
          {
            type: "inside",
          },
        ],
        series: [
          {
            type: "bar",
            showBackground: true,

            data: data,
          },
        ],
      };
      barChart.setOption(option);
      barChart.on("click", function (params) {
        console.log(dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)]);
        barChart.dispatchAction({
          type: "dataZoom",
          startValue: dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)],
          endValue:
            dataAxis[
              Math.min(params.dataIndex + zoomSize / 2, data.length - 1)
            ],
        });
      });
    });
  });
  return (
    <>
      <div ref={barRef} style={{ width: 250, height: 350 }} />
    </>
  );
};
