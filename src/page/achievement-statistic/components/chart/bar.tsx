import React, { useEffect, useRef } from "react";
import * as echarts from "echarts";

const dataAxis = ["体外科学", "体外诊断", "康复工程"];
const data = [220, 182, 191];
const yMax = 500;
let dataShadow = [];

for (let i = 0; i < data.length; i++) {
  dataShadow.push(yMax);
}

const zoomSize = 6;

const option = {
  title: {
    text: "专利——应用领域",
    // subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom'
  },
  xAxis: {
    data: dataAxis,
    axisLabel: {
      inside: true,
      textStyle: {
        color: "#fff",
      },
    },
    axisTick: {
      show: false,
    },
    axisLine: {
      show: false,
    },
    z: 10,
  },
  yAxis: {
    axisLine: {
      show: false,
    },
    axisTick: {
      show: false,
    },
    axisLabel: {
      textStyle: {
        color: "#999",
      },
    },
  },
  dataZoom: [
    {
      type: "inside",
    },
  ],
  series: [
    {
      type: "bar",
      showBackground: true,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          { offset: 0, color: "#83bff6" },
          { offset: 0.5, color: "#188df0" },
          { offset: 1, color: "#188df0" },
        ]),
      },
      emphasis: {
        itemStyle: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
            { offset: 0, color: "#2378f7" },
            { offset: 0.7, color: "#2378f7" },
            { offset: 1, color: "#83bff6" },
          ]),
        },
      },
      data: data,
    },
  ],
};
export default () => {
  let barRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    let barChart = echarts.init(barRef.current as HTMLDivElement);
    barChart.setOption(option);
    barChart.on("click", function (params) {
      console.log(dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)]);
      barChart.dispatchAction({
        type: "dataZoom",
        startValue: dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)],
        endValue:
          dataAxis[Math.min(params.dataIndex + zoomSize / 2, data.length - 1)],
      });
    });
  });
  return (
    <>
      <div ref={barRef} style={{ width: 250, height: 230 }}></div>
    </>
  );
};
