import React from "react";
import * as echarts from "echarts";
export default class AreaBar extends React.Component<any, any> {
  private myRef: React.RefObject<HTMLDivElement>;
  constructor(props: any) {
    super(props);
    this.myRef = React.createRef();
  }
  componentDidMount() {
    this.initCharts();
  }

  initCharts = () => {
    var myChart = echarts.init(this.myRef.current as HTMLDivElement);
    var option;
    option = {
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "shadow",
        },
      },
      title: {
        text: "专利——应用领域",
        textStyle: {
          color: "#235894",
        },
      },
      xAxis: {
        data: this.props.name,
      },
      yAxis: {
        name: "数量",
        type: "value",
      },
      series: [
        {
          name: "数量",
          type: "bar",
          data: this.props.number,
          itemStyle: {
            normal: {
              color: function (params: any) {
                var colorList = [
                  "#C1232B",
                  "#B5C334",
                  "#FCCE10",
                  "#E87C25",
                  "#27727B",
                  "#FE8463",
                  "#9BCA63",
                  "#FAD860",
                  "#F3A43B",
                  "#60C0DD",
                  "#D7504B",
                  "#C6E579",
                  "#F4E001",
                  "#F0805A",
                  "#26C0C0",
                ];
                return colorList[params.dataIndex];
              },
            },
          },
        },
      ],
    };

    option && myChart.setOption(option);
  };

  render() {
    return <div ref={this.myRef} style={{width: 250, height: 350}}></div>;
  }
}
