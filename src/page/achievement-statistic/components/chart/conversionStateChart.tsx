import React from "react";
import * as echarts from "echarts";
export default class ConversionStateChart extends React.Component<any, any> {
  private myRef: React.RefObject<HTMLDivElement>;
  constructor(props: any) {
    super(props);
    this.myRef = React.createRef();
  }
  componentDidMount() {
    this.initCharts();
  }

  initCharts = () => {
    var myChart = echarts.init(this.myRef.current as HTMLDivElement);
    var option;
    option = {
      title: {
        text: "专利——转化状态",
        textStyle: {
          color: "#235894",
        },
      },
      tooltip: {
        trigger: "item",
      },

      series: [
        {
          name: "转化状态",
          type: "pie",
          radius: "50%",
          data: this.props.conversionStateChart,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: "rgba(0, 0, 0, 0.5)",
            },
          },
        },
      ],
    };
    option && myChart.setOption(option);
  };

  render() {
    return <div ref={this.myRef} style={{width: 250, height: 250}}></div>;
  }
}
