import React, { lazy } from 'react';

const AchievementStatistics = lazy(() => import('./components/index'));

export default () => <AchievementStatistics />;

