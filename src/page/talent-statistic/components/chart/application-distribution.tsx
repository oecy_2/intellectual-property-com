import React, { useEffect, useRef } from "react";
import * as echarts from "echarts";
import axios from "axios";
import { ALL_TALENTS } from "../../../../constants/api-constants";

const basicOption: any = {
  title: {
    text: "人才——行业领域",
    textStyle: {
      color: "#235894",
    },
  },
  tooltip: {
    trigger: "item",
  },
  legend: {
    top: "10%",
    left: "center",
  },
  series: [
    {
      top: "10%",
      type: "pie",
      radius: ["40%", "70%"],
      avoidLabelOverlap: false,
      itemStyle: {
        borderRadius: 10,
        borderColor: "#fff",
        borderWidth: 2,
      },
      label: {
        show: false,
        position: "center",
      },
      emphasis: {
        label: {
          show: true,
          fontSize: "20",
          fontWeight: "bold",
        },
      },
      labelLine: {
        show: false,
      },
      data: [],
    },
  ],
};
interface DistributionProps {
  width: string;
  height: string;
}

export default (props: DistributionProps) => {
  const { width, height } = props;
  const distributionRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    let barChart = echarts.init(distributionRef.current as HTMLDivElement);
    // TODO:获取后端数据
    // ----
    axios
      .get(ALL_TALENTS)
      .then((r) => {
        console.log(r);
        const { mainData } = r.data;
        const map: any = {};
        mainData.forEach((item: any) => {
          let key = item.application_area.name;
          if (!map.hasOwnProperty(key)) {
            map[key] = 1;
          } else {
            map[key] = map[key] + 1;
          }
        });
        const keys = Object.keys(map);
        let data = [];
        for (let key of keys) {
          data.push({ name: key, value: map[key] });
        }
        basicOption.series[0].data = data;
        // ----
        barChart.setOption(basicOption);
      })
      .catch((e) => {
        console.log(e);
      });
  });

  return <div ref={distributionRef} style={{ width, height }}></div>;
};
