declare namespace ProjectList {
  interface item {
    key: number;
    defValue: any;
    name: string;
    productNo: string;
    model: string;
    organization: string;
    registrationStatus: string;
    fk_applicationArea: string;
    patents: any;
    organizationw: any;
    application_areas: any;
  }
}
