import { Button, Modal, Popconfirm, Space, Table, Row } from "antd";
import React, { useEffect, useState } from "react";
import Config from "./config";
import axios from "axios";
import {
  DELETE_PRODUCT,
  GET_PRODUCT_LIST,
} from "../../../../constants/api-constants";
import { useBoolean } from "ahooks";
import Detail from "./detail";
import FileSaver from "file-saver";
import xlsx from "node-xlsx";

export default () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([]);
  const [dataSource, setDataSource] = useState<ProjectList.item[]>();
  const [page, setPage] = useState(1);
  const [name, setName] = useState("");
  const [keyword, setKeyword] = useState("");
  const [application, setApplication] = useState("");
  const [date, setDate] = useState("");
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [
    isDetailShow,
    { setTrue: setDetailShow, setFalse: setDetailFalse },
  ] = useBoolean(false);
  const [record, setRecord] = useState<ProjectList.item>();
  const [mainData, setMainData] = useState();
  useEffect(() => {
    setLoading(true);
    axios
      .get(GET_PRODUCT_LIST, {
        params: { page, name, keyword, fk_applicationArea: application, date },
      })
      .then((r) => {
        const { mainData } = r.data;
        setCount(mainData.count);
        setMainData(mainData);
        const { patents } = mainData;
        const application_areas = mainData.application_area;
        const organizationw = mainData.organizations;
        const dataSource = mainData.rows.map((v: any, _i: number) => {
          const {
            name,
            model,
            productNo,
            unit,
            application_area,
            registrationStatus,
          } = v;
          return {
            defValue: v,
            key: v.id,
            name,
            model,
            productNo,
            organization: unit,

            registrationStatus,
            fk_applicationArea: application_area?.name,
            patents,
            organizationw,
            application_areas,
          };
        });
        setLoading(false);

        setDataSource(dataSource);
      })
      .catch((_r) => {
        setLoading(false);
      });
  }, [page, name, keyword, application, date]);

  const getList = () => {
    setLoading(true);
    axios
      .get(GET_PRODUCT_LIST, {
        params: { page, name, keyword, fk_applicationArea: application, date },
      })
      .then((r) => {
        const { mainData } = r.data;
        setCount(mainData.count);
        setMainData(mainData);
        const { patents } = mainData;
        const application_areas = mainData.application_area;
        const organizationw = mainData.organizations;
        const dataSource = mainData.rows.map((v: any, _i: number) => {
          const {
            name,
            model,
            productNo,
            unit,
            application_area,
            registrationStatus,
          } = v;
          return {
            defValue: v,
            key: v.id,
            name,
            productNo,
            model,
            organization: unit,
            registrationStatus,
            fk_applicationArea: application_area?.name,
            patents,
            organizationw,
            application_areas,
          };
        });
        setLoading(false);

        setDataSource(dataSource);
      })
      .catch((_r) => {
        setLoading(false);
      });
  };


  let columns = [
    {
      title: "产品名称",
      dataIndex: "name",
      key: "name",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "产品型号",
      dataIndex: "model",

      key: "model",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "所属单位",
      dataIndex: "organization",
      key: "organization",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "注册情况",
      dataIndex: "registrationStatus",
      key: "registrationStatus",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "应用领域",
      dataIndex: "fk_applicationArea",
      key: "fk_applicationArea",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "操作",
      render: (_value: any, record: any) => {
        return (
          <>
            <Space size={10}>
              <Button
                style={{ padding: 0 }}
                onClick={() => {
                  setDetailShow();
                  setRecord(record);
                }}
                type={"link"}
              >
                详情
              </Button>
              <Popconfirm
                title="确定删除?一经删除将无法恢复"
                onConfirm={() => {
                  axios
                    .post(DELETE_PRODUCT, {
                      id: [record.defValue.id],
                    })
                    .then((_r) => {
                      getList();
                    })
                    .catch((_r) => {
                      getList();
                    });
                }}
                okText="确定"
                cancelText="取消"
              >
                <Button style={{ padding: 0 }} onClick={() => { }} type={"link"}>
                  删除
                </Button>
              </Popconfirm>
            </Space>
          </>
        );
      },
    },
  ];

  if (window.localStorage.getItem('identity') === null) {
    columns = columns.filter(column => {
      return column.title !== "操作";
    });
  }

  return (
    <>
      <Config
        getList={getList}
        mainData={mainData}
        setDetailShow={setDetailShow}
        setRecord={setRecord}
        setDate={setDate}
        setName={setName}
        setApplication={setApplication}
        setPage={setPage}
        setKeyword={setKeyword}
      />
      <Table
        pagination={{
          current: page,
          showSizeChanger: false,
          pageSize: 10,
          total: count,
          onChange: (page, _pageSize) => {
            setLoading(true);
            setPage(page);
            setLoading(false);
          },
        }}
        loading={loading}
        size={"middle"}
        rowSelection={{
          selectedRowKeys,
          onChange: (value) => {
            setSelectedRowKeys(value);
          },
          selections: [],
        }}
        columns={columns}
        dataSource={dataSource}
        footer={() => {
          return (
            <>
              {
                window.localStorage.getItem('identity') === null ?
                  null
                  :
                  <Row justify="space-between">
                    <Space>
                      <Space>
                        <Button
                          onClick={() => {
                            if (dataSource) {
                              const data: string[][] = [
                                [
                                  "产品名称",
                                  "产品型号",
                                  "所属公司",
                                  "投资机构",
                                  "所在地",
                                  "医疗器械注册情况",
                                  "联系人姓名",
                                  "联系人电话",
                                  "联系人邮箱",
                                  "产品简介",
                                  "应用领域",
                                  "所用成果",
                                ],
                              ];
                              selectedRowKeys.forEach((v: number) => {
                                const item = dataSource.find(
                                  (value: ProjectList.item) => {
                                    return value.key === v;
                                  }
                                );
                                if (item && item.defValue) {
                                  let patent = "";
                                  item.defValue.patents.forEach(
                                    (v: { name: string }, i: any) => {
                                      if (i === 0) {
                                        patent += v.name;
                                      } else {
                                        patent += "," + v.name;
                                      }
                                    }
                                  );

                                  let organizations = "";
                                  item.defValue.patents.forEach(
                                    (v: { name: string }, i: any) => {
                                      if (i === 0) {
                                        organizations += v.name;
                                      } else {
                                        organizations += "," + v.name;
                                      }
                                    }
                                  );
                                  data.push([
                                    item.defValue.name,
                                    item.defValue.model,
                                    item.defValue.unit,
                                    organizations,
                                    item.defValue.address,

                                    item.defValue.registrationStatus,

                                    item.defValue.contactPerson,
                                    item.defValue.contactTel,
                                    item.defValue.contactEmail,
                                    item.defValue.description,
                                    item.defValue.application_area?.name,
                                    patent,
                                  ]);
                                }
                              });
                              if (data.length !== 1) {
                                const blob = new Blob(
                                  [xlsx.build([{ name: "output", data }])],
                                  {
                                    type: "application/octet-stream",
                                  }
                                );
                                FileSaver.saveAs(blob, "templateOut.xlsx");
                              }
                            }
                          }}
                        >
                          批量导出
                        </Button>
                        <Popconfirm
                          title="确定删除?一经删除将无法恢复"
                          onConfirm={() => {
                            const keys = selectedRowKeys.map((v: number) => {
                              if (dataSource) {
                                return v;
                              } else {
                                return 0;
                              }
                            });
                            axios
                              .post(DELETE_PRODUCT, {
                                id: keys,
                              })
                              .then((_r) => {
                                getList();
                              })
                              .catch((_r) => {
                                getList();
                              });
                          }}
                          okText="确定"
                          cancelText="取消"
                        >
                          <Button onClick={() => { }}>批量删除</Button>
                        </Popconfirm>
                      </Space>
                    </Space>
                  </Row>
              }
            </>
          );
        }}
      />

      <Modal
        title="项目查看"
        visible={isDetailShow}
        onCancel={setDetailFalse}
        width={"80vw"}
        footer={null}
        destroyOnClose
      >
        <Detail
          loading={loading}
          setLoading={setLoading}
          getList={getList}
          setDetailFalse={setDetailFalse}
          record={record}
        />
      </Modal>
    </>
  );
};
