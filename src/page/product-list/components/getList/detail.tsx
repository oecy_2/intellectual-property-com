import {
  Button,
  Cascader,
  Col,
  Form,
  Input,
  message,
  Row,
  Select,
  Space,
  Upload,
} from "antd";
import React, { useState } from "react";
import TextArea from "antd/lib/input/TextArea";
import {
  CloudDownloadOutlined,
  UploadOutlined,
} from "@ant-design/icons/lib/icons";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";

import {
  CREATE_PRODUCT,
  GET_PRODUCT_FILE,
  HAS_PRODUCT_EXIST,
  PRODUCT_FILE_EXIST,
  UPLOAD_PRODUCT,
} from "../../../../constants/api-constants";
import dateFnsGenerateConfig from "rc-picker/es/generate/dateFns";
import generatePicker from "antd/es/date-picker/generatePicker";
import "antd/es/date-picker/style/index";
import provice from "../../../../util/place";
import { useBoolean } from "ahooks";
// const fs = require("fs");
const DatePicker = generatePicker<Date>(dateFnsGenerateConfig);

const { Option } = Select;

interface detailProps {
  record?: ProjectList.item;
  setDetailFalse: Function;
  getList: Function;
  loading: boolean;
  setLoading: Function;
}

const options = provice;
export default (props: detailProps) => {
  const { record, setDetailFalse, getList, loading, setLoading } = props;
  const [submitLoading, {setFalse, setTrue}] = useBoolean(false);
  const [errors, setErrors] = useState("");
  if (!record) {
    return <></>;
  }
  const hasErr = !!errors;
  let filters = {};
  if (record && record.defValue) {
    const patent = record.defValue.patents?.map((v: any, _i: number) => {
      return v.uuid;
    });
    let address = "";
    let addressBefore = [];

    if (record && record.defValue && record.defValue.address) {
      const addAll = record.defValue.address?.split(";");
      address = addAll.pop();
      addressBefore = addAll;
    }
    filters = {
      name: record.name ? record.name : "",
      registrationStatus: record.registrationStatus
        ? record.registrationStatus
        : "",
      model: record.model ? record.model : "",
      productNo: record.productNo ? record.productNo : "",
      application_area: record.defValue.application_area?.uuid
        ? record.defValue.application_area?.uuid
        : "",
      organizations: record.defValue.organizations
        ? record.defValue.organizations.map((v: { uuid: string }) => {
            return v.uuid;
          })
        : [],
      address,
      addressBefore,
      patent,
      contactPerson: record.defValue.contactPerson
        ? record.defValue.contactPerson
        : "",
      contactTel: record.defValue.contactTel ? record.defValue.contactTel : "",
      contactEmail: record.defValue.contactEmail
        ? record.defValue.contactEmail
        : "",
      description: record.defValue.description
        ? record.defValue.description
        : "",
      updateTime: record.defValue.updateTime
        ? new Date(record.defValue.updateTime)
        : "",
      filler: record.defValue.filler ? record.defValue.filler : "",
      file: new File([], "null.xlsx"),
      company: record.defValue.unit ?? "",
    };
  } else {
    filters = {
      updateTime: new Date(),
      filler: localStorage.getItem("userName"),
    };
  }
  return (
    <Form
      onValuesChange={(changedValues, allValues) => {
        const len = Object.keys(changedValues).filter((v) => {
          return !(v === "name" || v === "model" || v === "company");
        }).length;
        console.log(changedValues);
        if (
          len === 0 &&
          allValues.name &&
          allValues.model &&
          allValues.company
        ) {
          axios
            .post(HAS_PRODUCT_EXIST, {
              name: allValues.name,
              model: allValues.model,
              unit: allValues.company,
            })
            .then((_r) => {
              setErrors("");
            })
            .catch((r) => {
              console.log(r);
              const { msg } = r.response.data;
              setErrors(msg);
            });
        }
      }}
      initialValues={filters}
      onFinish={(e) => {
        const formData = new FormData();
        // let patent = [];
        const addressBefore = e.addressBefore.join(";") + ";";

        if (record.defValue) {
          formData.append("id", record.defValue.id);
          formData.append("productNo", record.defValue.uuid);
        } else {
          formData.append("productNo", uuidv4());
        }
        formData.append("name", e.name);

        formData.append("registrationStatus", e.registrationStatus);
        formData.append("model", e.model);
        formData.append("application_area", e.application_area);
        formData.append("organization", e.organizations);
        formData.append("address", addressBefore + e.address);
        formData.append("patent", e.patent);
        formData.append("contactPerson", e.contactPerson);
        formData.append("contactEmail", e.contactEmail);
        formData.append("contactTel", e.contactTel);
        formData.append("description", e.description);
        formData.append("updateTime", e.updateTime);
        formData.append("filler", e.filler);
        formData.append("company", e.company);

        formData.append("file", e.file.file ?? new File([], "null.xlsx"));

        setTrue();

        if (record.defValue) {
          axios
            .post(UPLOAD_PRODUCT, formData)
            .then((_r) => {
              setFalse();

              getList();

              setDetailFalse();
            })
            .catch((e) => {
              setFalse();

              getList();
              setDetailFalse();
            });
        } else {
          axios
            .post(CREATE_PRODUCT, formData, { timeout: 50000 })
            .then((_r) => {
              setFalse();

              getList();

              setDetailFalse();
            })
            .catch((e) => {
              setFalse();
              message.error("产品编号重复或服务器异常请重试");
            });
        }
      }}
      layout="horizontal"
      title="机构信息"
    >
      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "产品名称不能为空" }]}
            name="name"
            label="产品名称 "
            validateStatus={hasErr ? "error" : "success"}
            help={errors}
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[{ required: true, message: "产品型号不能为空" }]}
            name="model"
            validateStatus={hasErr ? "error" : "success"}
            help={errors}
            label="产品型号"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={10}>
          <Form.Item
            name="company"
            label="所属公司 "
            validateStatus={hasErr ? "error" : "success"}
            help={errors}
            labelAlign="right"
            rules={[{ required: true, message: "所属公司不能为空" }]}
          >
            <Input
              placeholder={"请输入所属公司,多个用英文;分割。如：公司A;公司B"}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "投资机构不能为空" }]}
            name="organizations"
            label="投资机构"
          >
            <Select mode="multiple">
              {record.organizationw?.map(
                (v: { name: string; uuid: string }) => {
                  return (
                    <Option key={v.uuid} value={v.uuid}>
                      {v.name}
                    </Option>
                  );
                }
              )}
            </Select>
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item
            name="addressBefore"
            label="选择城市"
            labelAlign="right"
            rules={[{ required: true, message: "城市不能为空" }]}
          >
            <Cascader options={options} placeholder="请输入产品所在的城市"/>
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item
            name="address"
            label="具体街道"
            labelAlign="right"
            rules={[{ required: true, message: "街道不能为空" }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            name="patent"
            label="所用成果 "
            labelAlign="right"
            style={{ width: "100wh" }}
            rules={[{ required: true, message: "所用成果不能为空" }]}
          >
            <Select mode="tags">
              {record.patents?.map((v: { name: string; uuid: string }) => {
                return (
                  <Option key={v.uuid} value={v.uuid}>
                    {v.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
        </Col>

        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "注册情况不能为空" }]}
            name="registrationStatus"
            label="医疗器械注册情况"
          >
            <Select>
              <Option value={"已注册"}>已注册</Option>
              <Option value={"未注册"}>未注册</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="application_area"
            label="应用领域 "
            labelAlign="right"
            rules={[{ required: true, message: "应用领域不能为空" }]}
          >
            <Select>
              {record.application_areas.map(
                (v: { name: string; uuid: string }) => {
                  return (
                    <Option key={v.uuid} value={v.uuid}>
                      {v.name}
                    </Option>
                  );
                }
              )}
            </Select>
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "联系人姓名不能为空" }]}
            name="contactPerson"
            label="联系人姓名"
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            rules={[
              {
                pattern: /^1[3456789]\d{9}$/,
                message: "请输入正确的电话格式",
                required: true,
              },
            ]}
            name="contactTel"
            label="电话   "
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            rules={[
              {
                type: "email",
                message: "请输入正确的邮箱格式",
                required: true,
              },
            ]}
            name="contactEmail"
            label="E-Mail"
            labelAlign="right"
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item
        rules={[
          { required: true, message: "产品简介不能为空,并且保证长度小于100" },
        ]}
        name="description"
        label="产品简介"
        labelAlign="right"
      >
        <TextArea
          style={{ resize: "none" }}
          maxLength={100}
          placeholder="产品简介（限制在100字之内）"
        />
      </Form.Item>

      <Form.Item
        rules={[{ required: !record?.defValue, message: "请上传文件后再提交" }]}
        name="file"
        label="附件材料"
        labelAlign="right"
      >
        <Upload
          name="logo"
          beforeUpload={() => {
            return false;
          }}
          maxCount={1}
          accept={".pdf"}
        >
          <Button icon={<UploadOutlined />} type="primary">
            添加附件（PDF格式）
          </Button>
        </Upload>
      </Form.Item>
      {record?.defValue ? (
        <Form.Item name="getFile" label="附件材料" labelAlign="right">
          <Button
            loading={loading}
            onClick={() => {
              setLoading(true);
              axios
                .post(PRODUCT_FILE_EXIST, { id: record?.defValue?.id })
                .then((r) => {
                  const { msg } = r.data;
                  message.success(msg);

                  window.location.href =
                    GET_PRODUCT_FILE + `?id=${record?.defValue?.id}`;

                  setLoading(false);
                })
                .catch((e) => {
                  setLoading(false);
                });
            }}
            icon={<CloudDownloadOutlined />}
            type="primary"
          >
            下载已有附件
          </Button>
        </Form.Item>
      ) : (
        <></>
      )}

      <Row>
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: "录入时间不能为空" }]}
            name="updateTime"
            label="录入时间"
            labelAlign="right"
          >
            <DatePicker picker={"date"} disabled />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            rules={[{ required: true, message: "填写人不能为空" }]}
            name="filler"
            label="填写人"
            labelAlign="right"
          >
            <Input readOnly />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item>
        <Space style={{ float: "right" }}>
          <Button loading={ submitLoading } type="primary" htmlType="submit">
            提交
          </Button>
          <Button
            key="back"
            onClick={() => {
              setDetailFalse();
            }}
          >
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};
