import React, { useEffect, useState } from "react";
import {
  Button,
  Cascader,
  Col,
  DatePicker,
  Form,
  Input,
  Row,
  Select,
  Space,
  Upload,
} from "antd";
import locale from "antd/es/date-picker/locale/zh_CN";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";
import { UploadOutlined } from "@ant-design/icons";
import provice from "../../../../util/place";

const { Option } = Select;
const options = provice;
interface Props {
  formik: any;
  areaSelections: any[];
  setFalse: () => void;
  // organizationSelections: any[];
  talentSelections: any[];
  setPatentNumber: Function;
  exist: boolean;
  isClick: boolean;
  setClick: Function;
}
const Increase: React.FC<Props> = (props) => {
  const [form] = Form.useForm();

  const [applicationOrganization, setApplicationOrganization] = useState<
    string[]
  >([]);
  const {
    formik,
    areaSelections,
    talentSelections,
    setFalse,
    isClick,
    setClick,
  } = props;
  // let selectedArray: string[] = [];
  // 用户定义的申请人列表

  useEffect(() => {
    setApplicationOrganization(applicationOrganization);
  }, [applicationOrganization]);
  return (
    <Form
      onFinish={formik.handleSubmit}
      form={form}
      layout="horizontal"
      title="专利信息"
    >
      <Row justify="space-between" gutter={[24, 0]}>
        <Col flex={2}>
          <Form.Item
            label="专利名称"
            labelAlign="right"
            name="patentName"
            rules={[{ required: true, message: "请输入专利名称!" }]}
          >
            <Input
              id="patentName"
              maxLength={50}
              placeholder="请输入专利名称"
              onChange={formik.handleChange}
              value={formik.values.patentName}
            />
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item
            label="应用领域"
            labelAlign="right"
            name="applicationArea"
            rules={[{ required: true, message: "请选择应用领域!" }]}
          >
            <Select
              placeholder="请选择"
              allowClear
              value={formik.values.applicationArea}
              id="applicationArea"
              onChange={(e) => {
                formik.setFieldValue("applicationArea", e);
              }}
            >
              {(areaSelections ?? []).map((item: any) => (
                <Option key={item.uuid} value={item.uuid}>
                  {item.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item
            label="专利类型"
            labelAlign="right"
            name="type"
            rules={[{ required: true, message: "请选择专利类型!" }]}
          >
            <Select
              placeholder="请选择"
              style={{ width: 120 }}
              value={formik.values.type}
              onChange={(e) => formik.setFieldValue("type", e)}
              allowClear
            >
              <Option value="发明">发明</Option>
              <Option value="实用新型">实用新型</Option>
              <Option value="外观设计">外观设计</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Form.Item
        name="applicationOrganization"
        label="申请人"
        labelAlign="right"
        rules={[{ required: true, message: "申请人不能为空!" }]}
      >
        <Select
          placeholder="请填写专利申请人（申请人可以为单位，也可以为自然人）"
          mode="tags"
          onChange={(e) => {
            formik.setFieldValue("applicationOrganization", e);
          }}
          // value={formik.values.applicationOrganization}
        ></Select>
      </Form.Item>

      <Form.Item
        label="发明人员"
        labelAlign="right"
        name="talents"
        rules={[{ required: true, message: "发明人员不能为空!" }]}
      >
        <Select
          // mode='multiple'
          mode="tags"
          allowClear
          value={formik.values.talents}
          placeholder="请输入发明人员"
          id="talents"
          onChange={(e) => {
            formik.setFieldValue("talents", e);
          }}
          filterOption={(inputValue, options) => {
            return options?.key?.toString()?.includes(inputValue) ?? false;
          }}
        >
          {(talentSelections ?? []).map((item: any) => (
            <Option key={`${item.name}-${item.id}`} value={item.id}>
              {item.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Row justify="start" gutter={[24, 0]}>
        <Col flex={1}>
          <Form.Item
            label="转化方式"
            labelAlign="right"
            name="conversionMethod"
            rules={[{ required: true, message: "请选择转化方式!" }]}
          >
            <Select
              placeholder="请选择"
              style={{ width: 120 }}
              value={formik.values.conversionMethod}
              onChange={(e) => formik.setFieldValue("conversionMethod", e)}
              allowClear
            >
              <Option value="产权买断">产权买断</Option>
              <Option value="技术转让">技术转让</Option>
              <Option value="未确定">未确定</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item
            label="转化状态"
            labelAlign="right"
            name="conversionType"
            rules={[{ required: true, message: "请选择转化状态!" }]}
          >
            <Select
              placeholder="请选择"
              style={{ width: 120 }}
              value={formik.values.conversionType}
              onChange={(e) => formik.setFieldValue("conversionType", e)}
              allowClear
            >
              <Option value="已转化">已转化</Option>
              <Option value="待转化">待转化</Option>
              <Option value="未确定">未确定</Option>
            </Select>
          </Form.Item>
        </Col>
        
        <Col flex={1}>
          <Form.Item
            name="addressBefore"
            label="选择城市"
            labelAlign="right"
            rules={[{ required: true, message: "城市不能为空" }]}
          >
            <Cascader
              options={options}
              id="addressBefore"
              onChange={(e) => formik.setFieldValue("addressBefore", e)}
              value={formik.values.addressBefore}
              placeholder="请输入专利所在的城市"
            />
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item
            name="address"
            label="具体街道"
            labelAlign="right"
            rules={[{ required: true, message: "街道不能为空" }]}
          >
            <Input
              id="address"
              maxLength={50}
              placeholder="请输入具体街道"
              onChange={formik.handleChange}
              value={formik.values.address}
            />
          </Form.Item>
        </Col>
        {/* <Form.Item
            label='地址'
            labelAlign='right'
            name='address'
            rules={[{required: true, message: "专利地址不能为空!"}]}
          >
            <Input
              id='address'
              maxLength={50}
              placeholder='请输入专利地址'
              onChange={formik.handleChange}
              value={formik.values.address}
            />
          </Form.Item> */}
      </Row>

      <Row justify="space-between">
        <Col>
          <Form.Item
            label="法律状态"
            labelAlign="right"
            name="legalStatus"
            rules={[{ required: true, message: "请选择法律状态!" }]}
          >
            <Select
              placeholder="请选择"
              value={formik.values.legalStatus}
              id="legalStatus"
              onChange={(e) => {
                formik.setFieldValue("legalStatus", e);
              }}
              style={{ width: 120 }}
              allowClear
            >
              <Option value="公开">公开</Option>
              <Option value="授权">授权</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col>
          <Form.Item
            label="公开号"
            labelAlign="right"
            name="publicNumber"
            rules={[{ required: true, message: "公开号不能为空!" }]}
          >
            <Input
              id="publicNumber"
              placeholder="请输入公开号"
              onChange={formik.handleChange}
              value={formik.values.publicNumber}
            />
          </Form.Item>
        </Col>
        <Col>
          <Form.Item
            label="申请号"
            labelAlign="right"
            name="applicationNumber"
            //hasFeedback
            help={
              props.exist
                ? "此申请号已存在"
                : isClick && formik.values.applicationNumber === ""
                ? "申请号不能为空!"
                : ""
            }
            validateStatus={
              (isClick && formik.values.applicationNumber) === "" || props.exist
                ? "error"
                : "success"
            }
            rules={[{ required: true, message: "申请号不能为空!" }]}
          >
            <Input
              onClick={() => {
                setClick(true);
              }}
              id="applicationNumber"
              placeholder="请输入专利的申请号"
              maxLength={50}
              value={formik.values.applicationNumber}
              onChange={(e) => {
                props.setPatentNumber(e.target.value);
                formik.handleChange(e);
              }}
            />
          </Form.Item>
        </Col>
        <Col>
          <Form.Item
            label="申请日"
            labelAlign="right"
            name="applicationTime"
            rules={[{ required: true, message: "请选择申请日!" }]}
          >
            <DatePicker
              locale={locale}
              id="applicationTime"
              value={
                formik.values.applicationTime
                  ? moment(formik.values.applicationTime)
                  : null
              }
              onChange={(e) => {
                formik.setFieldValue(
                  "applicationTime",
                  `${e?.toDate().getFullYear()}/${
                    e!.toDate().getMonth() + 1
                  }/${e?.toDate().getDate()}`
                );
              }}
            />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item
        label="专利简介"
        labelAlign="right"
        name="description"
        rules={[
          {
            required: true,
            message: "请填写专利简介（100字以内）！",
            max: 100,
          },
        ]}
      >
        <TextArea
          style={{ resize: "none" }}
          maxLength={100}
          placeholder="专利简介（100字以内）"
          value={formik.values.description}
          id="description"
          onChange={formik.handleChange}
        />
      </Form.Item>
      <Form.Item label="附件材料" labelAlign="right">
        <Upload
          name="logo"
          beforeUpload={(file) => {
            formik.setFieldValue("fileList", [file]);
            return false;
          }}
          maxCount={1}
          accept={".pdf"}
        >
          <Button icon={<UploadOutlined />} type="primary">
            添加附件（PDF格式）
          </Button>
        </Upload>
      </Form.Item>
      <Form.Item
        label="技术优势"
        labelAlign="right"
        name="technicalAdvantages"
        rules={[
          {
            required: true,
            message: "请填写技术优势（100字以内）！",
            max: 100,
          },
        ]}
      >
        <TextArea
          style={{ resize: "none" }}
          maxLength={100}
          placeholder="专利技术优势（100字以内）"
          value={formik.values.technicalAdvantages}
          onChange={formik.handleChange}
          id="technicalAdvantages"
        />
      </Form.Item>
      <Form.Item>
        <Space style={{ float: "right" }}>
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => {
              setClick(true);
            }}
          >
            提交
          </Button>
          <Button
            key="back"
            onClick={() => {
              setFalse();
              setClick(false);
              props.setPatentNumber("");
            }}
          >
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default Increase;
