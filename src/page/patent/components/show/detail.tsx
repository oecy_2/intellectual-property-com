import React from "react";
import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  message,
  Row,
  Select,
  Upload,
  Space,
  Cascader,
} from "antd";
import FileSaver from "file-saver";
import axios from "axios";
import { useBoolean } from "ahooks";
import TextArea from "antd/lib/input/TextArea";
import { CloudDownloadOutlined, UploadOutlined } from "@ant-design/icons";
import {
  GET_PATENT_ATTACHMENT,
  PATENT_FILE_EXIST,
  UPDATE_PATENT,
} from "src/constants/api-constants";

const { Option } = Select;

interface Props {
  curDetail: any;
  curDetailId: number;
  areaSelections: any;
  setFalse: Function;
  // talentSelections: any[];
  refresh: any;
  // currentTalents: string;
}
const Detail: React.FC<Props> = (props) => {
  const {
    curDetail,
    areaSelections,
    // currentTalents,
    refresh,
    curDetailId,
    setFalse,
    // talentSelections,
  } = props;
  const [loading, { setTrue: setLoading, setFalse: setUnLoading }] = useBoolean(
    false
  );
  const [
    loading2,
    { setTrue: setLoading2, setFalse: setUnLoading2 },
  ] = useBoolean(false);
  return (
    <Form
      layout="horizontal"
      title="专利"
      initialValues={{ ...curDetail, talents: curDetail?.currentTalents }}
      onFinish={(e) => {
        const formData = new FormData();
        formData.append("id", String(curDetailId));
        formData.append("applicationArea", e.applicationArea);
        formData.append("conversionMethod", e.conversionMethod);
        formData.append("conversionType", e.conversionType);
        formData.append("description", e.description);
        formData.append("technicalAdvantages", e.technicalAdvantages);
        formData.append(
          "file",
          e.file ? e.file.file : new File([], "null.pdf")
        );
        setLoading2();
        axios
          .post(UPDATE_PATENT, formData)
          .then((r) => {
            if (!r.data.mainData.exist) {
              message.info("更新成功");
            } else {
              message.error(r.data.msg);
            }
          })
          .catch((e) => {
            message.error("更新失败");
            console.log(e);
          })
          .finally(() => {
            setFalse();
            refresh();
            setUnLoading2();
          });
      }}
    >
      <Row justify="space-between" gutter={[24, 0]}>
        <Col flex={2}>
          <Form.Item label="专利名称" labelAlign="right" name="patentName">
            <Input id="patentName" maxLength={50} disabled />
          </Form.Item>
        </Col>

        <Col flex={1}>
          <Form.Item label="专利类型" labelAlign="right" name="type">
            <Select
              defaultValue="专利类型"
              placeholder="请选择"
              style={{ width: 120 }}
              allowClear
              disabled
            >
              <Option value="发明">发明</Option>
              <Option value="实用新型">实用新型</Option>
              <Option value="外观设计">外观设计</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item label="应用领域" labelAlign="right" name="applicationArea">
            <Select allowClear placeholder="请选择" id="applicationArea">
              {(areaSelections ?? []).map((item: any) => (
                <Option key={item.uuid} value={item.uuid}>
                  {item.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Form.Item label="申请人" labelAlign="right" name="currentOrganization">
        <Input
          id="currentOrganization"
          placeholder="请输入申请人"
          disabled
          value={curDetail.applicationOrganization}
        />
      </Form.Item>
      <Form.Item label="发明人员" labelAlign="right" name="talents">
        <Input
          id="currentTalents"
          placeholder="请输入发明人员"
          value={curDetail.currentTalents}
          disabled
        />
      </Form.Item>
      <Row justify="start" gutter={[24, 0]}>
        <Col flex={1}>
          <Form.Item
            label="转化方式"
            labelAlign="right"
            name="conversionMethod"
          >
            <Select defaultValue="产权买断" style={{ width: 120 }} allowClear>
              <Option value="产权买断">产权买断</Option>
              <Option value="技术转让">技术转让</Option>
              <Option value="未确定">未确定</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item label="转化状态" labelAlign="right" name="conversionType">
            <Select defaultValue="待转化" style={{ width: 120 }} allowClear>
              <Option value="已转化">已转化</Option>
              <Option value="待转化">待转化</Option>
              <Option value="未确定">未确定</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item
            name="addressBefore"
            label="选择城市"
            labelAlign="right"           
          >
            <Cascader
              disabled
              id="addressBefore"
              value={curDetail.addressBefore}
            />
          </Form.Item>
        </Col>
        <Col flex={1}>
          <Form.Item
            name="address"
            label="具体街道"
            labelAlign="right"
          >
            <Input
              disabled
              id="address"
              maxLength={50}
              placeholder="请输入具体街道"
              value={curDetail.address}
            />
          </Form.Item>
        </Col>
        {/* <Col flex={3}>
          <Form.Item label="地址" labelAlign="right" name="address">
            <Input id="address" maxLength={50} disabled />
          </Form.Item>
        </Col> */}
      </Row>

      <Row justify="space-between">
        <Col>
          <Form.Item label="法律状态" name="legalStatus">
            <Select
              defaultValue="公开"
              value={curDetail.legalStatus}
              id="legalStatus"
              style={{ width: 120 }}
              disabled
              allowClear
            >
              <Option value="公开">公开</Option>
              <Option value="授权">授权</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col>
          <Form.Item label="公开号" labelAlign="right" name="publicNumber">
            <Input id="publicNumber" placeholder="请输入公开号" disabled />
          </Form.Item>
        </Col>
        <Col>
          <Form.Item label="申请号" name="applicationNumber">
            <Input id="applicationNumber" maxLength={50} disabled />
          </Form.Item>
        </Col>
        <Col>
          <Form.Item label="申请日" name="applicationTime">
            <DatePicker id="applicationTime" disabled />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item label="专利简介" labelAlign="right" name="description">
        <TextArea
          style={{ resize: "none" }}
          maxLength={100}
          placeholder="成果介绍"
          id="description"
        />
      </Form.Item>
      <Form.Item name="file" label="附件材料" labelAlign="right">
        <Upload
          name="logo"
          beforeUpload={() => {
            return false;
          }}
          maxCount={1}
          accept={".pdf"}
        >
          <Button icon={<UploadOutlined />} type="primary">
            添加附件（PDF格式）
          </Button>
        </Upload>
      </Form.Item>
      <Form.Item label="附件材料" labelAlign="right">
        <Button
          loading={loading}
          onClick={() => {
            setLoading();
            axios
              .post(PATENT_FILE_EXIST, { id: curDetailId })
              .then((r) => {
                const { msg, mainData } = r.data;
                message.success(msg);
                FileSaver.saveAs(
                  GET_PATENT_ATTACHMENT + `?id=${curDetailId}`,
                  mainData?.name
                );
              })
              .catch((e) => {
                console.log(e);
              })
              .finally(() => {
                setUnLoading();
              });
          }}
          icon={<CloudDownloadOutlined />}
          type="primary"
        >
          下载已有附件
        </Button>
      </Form.Item>
      <Form.Item label="技术优势" labelAlign="right" name="technicalAdvantages">
        <TextArea
          style={{ resize: "none" }}
          maxLength={100}
          placeholder="技术优势"
          id="technicalAdvantages"
        />
      </Form.Item>
      <Form.Item>
        <Space style={{ float: "right" }}>
          <Button type="primary" htmlType="submit" loading={loading2}>
            提交
          </Button>
          <Button
            key="back"
            onClick={() => {
              setFalse();
            }}
          >
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default Detail;
