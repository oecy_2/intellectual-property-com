import React, { lazy } from 'react';

const PatentList = lazy(() => import('./components/show'));

export default () => <PatentList />;
