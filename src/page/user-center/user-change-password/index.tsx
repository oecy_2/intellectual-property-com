import React, { lazy } from 'react';

const UserChangePassword = lazy(() => import('./components'));

export default () => <UserChangePassword />;
