import React, { lazy } from 'react';

const UserInfo = lazy(() => import('./components'));

export default () => <UserInfo />;
