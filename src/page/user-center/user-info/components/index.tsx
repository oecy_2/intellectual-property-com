import React, {useEffect, useState} from "react";
import axios from "axios";
import {useBoolean} from "ahooks";
import {
  Row,
  Col,
  Button,
  Modal,
  Select,
  Form,
  Space,
  Input,
  message,
  Cascader,
} from "antd";
import {
  USER_INFO,
  GLOBAL_SELECTIONS,
  CHANGE_USER_INFO,
} from "src/constants/api-constants";
import provice from "../../../../util/place";

const options = provice;
const {Option} = Select;

interface UserInfo {
  identity?: any;
  position?: {name: string};
  title?: {name: string};
  tel?: string;
  department?: string;
  education?: string;
  name?: string;
  post?: string;
  address?: string;
  sex?: number;
  major?: string;
  organization?: any;
}

const UserInfo = () => {
  const [curUser, setCurUser] = useState<UserInfo>({});
  const [detailShow, {setTrue: show, setFalse: unshow}] = useBoolean(false);
  const [organizationOptions, setOrganizationOptions] = useState<any>([]);
  const [educationOptions, setEducationOptions] = useState<any>([]);
  const [positionOptions, setPositionOptions] = useState<any>([]);
  const [titleOptions, setTitleOptions] = useState<any>([]);
  const [form] = Form.useForm();
  const formLayout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const getUserInfo = () => {
    const id = localStorage.getItem("CurrentUserId");
    console.log(id);
    axios
      .post(USER_INFO, {
        id,
      })
      .then(res => {
        console.log(res);

        setCurUser(res.data.mainData);
      })
      .catch(e => {
        console.log(e);
      });
  };
  useEffect(() => {
    axios
      .get(GLOBAL_SELECTIONS)
      .then(r => {
        const {mainData} = r.data;

        const titleOptions = mainData.titleType.map((item: any) => {
          return (
            <Option key={item.name} value={item.name}>
            {item.name}
          </Option>
          );
        });
        const positionOptions = mainData.titleType.map((item: any) => {
          return (
            <Option key={item.name} value={item.name}>
              {item.name}
            </Option>
          );
        });
        const educationOptions = mainData.positionType.map((item: any) => {
          return (
            <Option key={item} value={item}>
              {item}
            </Option>
          );
        });
        setPositionOptions(positionOptions);
        setTitleOptions(titleOptions);
        setEducationOptions(educationOptions);
        setOrganizationOptions(organizationOptions);
      })
      .catch(e => {
        console.log("e", e);
        message.error("信息获取失败");
      });
    getUserInfo();
  }, [organizationOptions]);

  return (
    <div>
      <Row align='middle' justify='space-between'>
        <span
          style={{
            fontWeight: "bolder",
            color: "rgb(102,102,102)",
            fontSize: "1.5em",
          }}
        >
          用户基本信息
        </span>
        <Button type='primary' onClick={show} size='large'>
          编辑
        </Button>
      </Row>
      <Row gutter={[32, 16]} style={{marginBottom: "16px"}}>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>手机号:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.tel}</span>
        </Col>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>性别:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>
            {curUser.sex === 1 ? "男" : curUser.sex === 0 ? "女" : ""}
          </span>
        </Col>
      </Row>
      <Row gutter={[32, 16]} style={{marginBottom: "16px"}}>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>姓名:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.name}</span>
        </Col>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>职务:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.position?.name}</span>
        </Col>
      </Row>
      <Row gutter={[32, 16]} style={{marginBottom: "16px"}}>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>职称:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.title?.name}</span>
        </Col>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>所属机构:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.department}</span>
        </Col>
      </Row>
      <Row gutter={[32, 16]} style={{marginBottom: "16px"}}>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>学历 :</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.education}</span>
        </Col>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>专业:</span>
        </Col>
        <Col xs={16} sm={6}>
          <span>{curUser.major}</span>
        </Col>
      </Row>
      <Row gutter={[32, 16]} style={{marginBottom: "16px"}}>
        <Col xs={8} sm={2}>
          <span style={{float: "right"}}>联系地址:</span>
        </Col>
        <Col span='12'>
          <span>{curUser.address}</span>
        </Col>
      </Row>

      <Modal
        visible={detailShow}
        footer={null}
        onCancel={unshow}
        onOk={unshow}
        width='60vw'
        destroyOnClose
      >
        <h2>修改用户信息</h2>
        <Form
          form={form}
          {...formLayout}
          onFinish={e => {
            console.log(e);
            let keys = Object.keys(e);
            for (let key of keys) {
              if (key === "file") continue;
              if (e[key] === undefined || e[key] === "") {
                message.error("请完善信息");
                return;
              }
            }
            axios
              .post(CHANGE_USER_INFO, {
                id: localStorage.getItem("CurrentUserId"),
                ...e,
              })
              .then(r => {
                console.log(r);
                if (r.data.mainData.upload === true) {
                  message.info("更新成功");
                } else {
                  message.error("更新失败");
                }
              })
              .catch(e => {
                console.log(e);
                message.error("更新失败");
              })
              .finally(() => {
                getUserInfo();
                unshow();
              });
          }}
        >
          <Form.Item name='name' label='姓名' initialValue={curUser.name}>
            <Input></Input>
          </Form.Item>
          <Form.Item name='sex' label='性别' initialValue={curUser.sex}>
            <Select>
              <Option value={1}>男</Option>
              <Option value={0}>女</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name='post'
            label='职务'
            initialValue={curUser.position?.name}
          >
            <Select>{positionOptions}</Select>
          </Form.Item>
          <Form.Item
            name='role'
            label='职称'
            initialValue={curUser.title?.name}
          >
            <Select>{titleOptions}</Select>
          </Form.Item>
          <Form.Item
            name='department'
            label='所属机构'
            initialValue={curUser.department}
          >
            <Input></Input>
          </Form.Item>
          <Form.Item
            name='education'
            label='学历'
            initialValue={curUser.education}
          >
            <Select>{educationOptions}</Select>
          </Form.Item>
          <Form.Item name='major' label='专业' initialValue={curUser.major}>
            <Input></Input>
          </Form.Item>
          <Form.Item
            name='address'
            label='联系地址'
            initialValue={curUser.address?.split(";")}
          >
            <Cascader options={options} placeholder='请输入用户居住城市' />
          </Form.Item>
          <Form.Item>
            <div
              style={{
                display: "flex",
                flexDirection: "row-reverse",
                width: "45vw",
              }}
            >
              <Space>
                <Button htmlType='submit'>提交</Button>
                <Button onClick={unshow}>取消</Button>
              </Space>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
export default UserInfo;
