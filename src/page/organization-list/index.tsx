import React, { lazy } from 'react';

const OrganizationList = lazy(() => import('./components'));

// 整体组件逻辑,lazy等等.
export default () => <OrganizationList />;
