import React, { useEffect, useState } from "react";
import Config from "./config";
import { useBoolean } from "ahooks";
import {
  Button,
  Cascader,
  Col,
  DatePicker,
  Divider,
  Dropdown,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Row,
  Select,
  Space,
  Table,
  Upload,
} from "antd";
import * as APIS from "../../../constants/api-constants";
import {
  FINANCIAL_FILE_EXIST,
  GET_ORGANIZATION_FILE,
  UPLOAD_ORGANIZATION_FILE,
} from "../../../constants/api-constants";

import {
  CloudDownloadOutlined,
  InboxOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import Column from "antd/lib/table/Column";
import TextArea from "antd/lib/input/TextArea";
import axios from "axios";
import dayjs from "dayjs";
import moment from "moment";
import FileSaver from "file-saver";
import xlsx from "node-xlsx";
import Dragger from "antd/lib/upload/Dragger";
import provice from "../../../util/place";

const { Option } = Select;
const options = provice;

export default () => {
  const [isShow, { setTrue, setFalse }] = useBoolean(false);
  const [showDetail, setShowDetail] = useState(false);
  const [name, setName] = useState("");
  const [financialCode, setFinancialCode] = useState("");
  const [type, setType] = useState("");
  const [address, setAddress] = useState("");
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(1);
  const [dataSource, setDataSource] = useState<PT.financialList[]>([]);
  const [financialInfo, setFinancialInfo] = useState<PT.financialList>();
  const [creatTime, setCreatTime] = useState("");
  const [invest, setInvest] = useState<string[]>();
  const [contactPerson, setContactPerson] = useState("");
  const [contactTel, setContactTel] = useState("");
  const [contactEmail, setContactEmail] = useState("");
  const [id, setId] = useState<number>();
  const [financialName, setFinancialName] = useState("");
  const [financialAddress, setFinancialAddress] = useState("");
  const [financialType, setFinancialType] = useState("");
  const [updateTime, setUpdateTime] = useState("");
  const [filler, setFiller] = useState("admin");
  const [TypeInfo, setTypeInfo] = useState<{ name: string; uuid: string }[]>([]);
  const [fileList, setFileList] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const [investInfo, setInvestInfo] = useState<string[]>([]);
  const [description, setDescription] = useState("");
  const [change, setChange] = useState(true);
  const [key, setKey] = useState<string[]>([]);
  const [exist, setExist] = useState(true);
  const [isClick, setClick] = useState(false);
  const onSelectChange = (selectedRowKeys: any) => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    setKey(selectedRowKeys);
  };
  const rowSelection = {
    onChange: onSelectChange,
  };
  useEffect(() => {
    axios
      .get(APIS.CHECKFINANCIALFILE, {
        params: { financialCode },
      })
      .then(r => {
        const { mainData, msg } = r.data;
        console.log(msg);
        console.log("checkExistMaindata:", mainData);
        setExist(mainData.state);
      })
      .catch(() => {
        message.error('服务器异常，请刷新重试！');
      });
  }, [financialCode]);
  useEffect(() => {
    axios
      .get(APIS.FINANCIAL_LIST, {
        params: { page, name, type, address },
      })
      .then(r => {
        const { mainData, msg } = r.data;
        console.log(msg);
        console.log("mainData:", mainData);
        setCount(mainData.count);
        mainData.projectDataSource.map((info: PT.financialList) => {
          info.typeName = info.organization_type.name;
          info.invest = info.investment_directions.map(
            (direct: PT.direction) => {
              return direct.name;
            }
          );
          info.showinvest = info.investment_directions.map(
            (direct: PT.direction) => {
              return direct.name + ";";
            }
          );
          info.key = info.id;
          info.createTime = dayjs(info.createTime).format("YYYY/MM/DD");
          return "";
        });

        setDataSource(mainData.projectDataSource);
        const user = localStorage.getItem("userName");
        if (user !== null) {
          setFiller(user);
        } else setFiller("admin");
      })
      .catch(() => {
        message.error('服务器异常，请刷新重试！');
      });
  }, [page, name, type, address, change]);
  useEffect(() => {
    axios
      .get(APIS.SELECT_LIST)
      .then(r => {
        const { mainData, msg } = r.data;
        console.log(msg);
        console.log("mainData:", mainData);
        setTypeInfo(
          mainData.type.map((type: { name: any; uuid: any }) => {
            return { name: type.name, uuid: type.uuid };
          })
        );
        setInvestInfo(
          mainData.invest.map((type: { name: any }) => {
            return type.name;
          })
        );
      })
      .catch(() => {
        message.error('服务器异常，请刷新重试！');
      });
  }, []);
  const updatefinancial = (e: { addressBefore: any[] }) => {
    const addressBefore = e.addressBefore.join(";") + ";";
    console.log("financialAddress", financialAddress);
    console.log("addressBefore", addressBefore);
    axios
      .post(APIS.UPDATE_FINANCIAL, {
        id,
        financialName,
        financialAddress: addressBefore + financialAddress,
        financialType,
        creatTime,
        invest,
        contactPerson,
        contactTel,
        contactEmail,
        filler,
        updateTime,
        description,
      })
      .then(r => {
        const { mainData, msg } = r.data;
        console.log(msg);
        console.log("changedmainData:", mainData);
        setChange(!change);
      })
      .catch(() => { });
  };

  const creatfinancial = (e: { addressBefore: any[] }) => {
    console.log(fileList);
    const formData = new FormData();
    const invests = invest?.join(",");
    const addressBefore = e.addressBefore.join(";") + ";";
    formData.append("financialAddress", addressBefore + financialAddress);
    formData.append("filler", filler);
    formData.append("updateTime", updateTime);
    formData.append("description", description);
    formData.append("contactPerson", contactPerson);
    formData.append("contactEmail", contactEmail);
    formData.append("contactTel", contactTel);
    formData.append("invest", invests ?? "");
    formData.append("creatTime", creatTime);
    formData.append("financialType", financialType);
    formData.append("financialName", financialName);
    formData.append("financialCode", financialCode);
    formData.append("file", fileList[0] ?? new File([], "null.xlsx"));

    axios
      .post(APIS.CREATE_FINANCIAL, formData)
      .then(r => {
        const { mainData, msg } = r.data;
        console.log("新增结果", msg);
        console.log("mainData:", mainData);
        setClick(false);
        setFinancialCode("");
        setChange(!change);
        mainData.state === "true"
          ? message.success("新增成功", 6)
          : message.error("新增失败，此机构已存在", 6);
      })
      .catch(() => { });
  };
  const deleteFinancial = () => {
    axios
      .post(APIS.DELETE_FINANCIAL, {
        key,
      })
      .then(r => {
        message.success('删除成功');
        setKey([]);
        setChange(!change);
      })
      .catch(() => {
        console.log('error');
      });
  };
  let add = "";
  let before: string[] = [];
  if (financialInfo?.address) {
    const address = financialInfo?.address?.split(";");
    const adr = address.pop();
    if (adr) {
      add = adr;
      before = address;
    }
  }
  const tmpArray = [
    "机构名称",
    "组织结构代码",
    "机构地址",
    "机构类型",
    "成立时间",
    "投资方向",
    "联系人",
    "Email",
    "电话",
    "机构简介",
    "（前三行不可删除）",
  ];
  const uploadProps: any = {
    name: "file",
    action: APIS.UPLOADFINANCIALFILE,
    data: { username: localStorage.getItem("userName") },
    headers: { Authorization: localStorage.getItem("Authorization") },
    beforeUpload(file: File) {
      return new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsArrayBuffer(file);
        reader.onload = () => {
          if (reader.result && typeof reader.result !== "string") {
            const buf = reader.result;
            const parse = xlsx.parse(buf, { type: "buffer" });
            const { data } = parse[0];
            let dataFilter = data.filter((v: any[], i: number) => {
              if (i !== 0 && i !== 2) {
                for (let index = 0; index <= 9; index++) {
                  if (!v[index] || v[index] === 0) {
                    return false;
                  }
                }

                return true;
              }
              return true;
            });
            if (JSON.stringify(data[0]) !== JSON.stringify(tmpArray)) {
              message.error(
                "上传文件中首行不正确请重试(请下载模板文件查看)",
                15
              );
            } else if (dataFilter.length !== data.length) {
              const blob = new Blob(
                [xlsx.build([{ name: "template", data: dataFilter }])],
                {
                  type: "application/octet-stream",
                }
              );
              message.info("返回文件为上传的有效值", 15);
              resolve(blob);
              FileSaver.saveAs(blob, "有效值.xlsx");
            } else {
              const blob = new Blob(
                [xlsx.build([{ name: "template", data: dataFilter }])],
                {
                  type: "application/octet-stream",
                }
              );
              message.success("文件检查通过,请等待后端验证并返回结果", 6);
              resolve(blob);
            }
          } else {
            message.error("文件读取失败请检查格式");
          }
        };
      });
    },
    onChange(info: {
      file: {
        response: { true: number; false: number; errorData: any[][] };
        status: string;
        name: any;
      };
      fileList: any;
    }) {
      if (info.file.status === "done") {
        setPage(1);
        message.success(`${info.file.name} 上传成功.`);
        message.success(`${info.file.response.true}条金融机构上传成功。`);

        setChange(!change);
      } else if (info.file.status === "error") {
        setPage(1);
        message.error("上传失败");
      }
    },
  };
  // let filters = {
  //   discription: "",
  //   Email: " ",
  //   prople: " ",
  //   tel: " ",
  //   invest: " ",
  //   createTime: " ",
  //   type: " ",
  //   address: " ",
  //   name: "aa",
  // };

  const menu = (
    <div
      style={{
        background: "rgb(251,251,251)",
        padding: "12px",
        boxShadow: "3px 3px 3px 3px rgb(193,193,195)",
        borderRadius: "3px",
      }}
    >
      <Button
        style={{
          overflow: "hidden",
          whiteSpace: "normal",
          wordBreak: "break-all",
          fontSize: "0.9rem",
          padding: "0",
          width: "10vw",
          height: "auto",
          marginBottom: "2vh",
        }}
        onClick={() => {
          const data: string[][] = [
            [
              "机构名称",
              "组织结构代码",
              "机构地址",
              "机构类型",
              "成立时间",
              "投资方向",
              "联系人",
              "Email",
              "电话",
              "机构简介",
              "（前三行不可删除）",
            ],
            [
              "请输入机构名称",
              "请输入组织结构代码",
              "请输入机构地址，用英文;分割（地址格式为：xx省;xx市;xx区;xxx）",
              "请输入机构类型，请在（国有企业，私有企业）中选择填写",
              "请输入成立时间（示例：2021/7/31）",
              "请在（房地产，生物医药，新能源）中选择填写，多个用英文;分割（示例：房地产;生物医药）",
              "请填写企业联系人姓名",
              "请填写企业联系email",
              "请填写企业联系电话",
              "请填写机构简介（100字以内）",
            ],
            ["请从下一行开始填写录入数据"],
          ];

          const blob = new Blob([xlsx.build([{ name: "Financial", data }])], {
            type: "application/octet-stream",
          });
          FileSaver.saveAs(blob, "导入模板.xlsx");
        }}
      >
        下载导入模板
      </Button>
      <Dragger accept={".xlsx"} {...uploadProps} style={{ padding: "0 1vw" }}>
        <p className='ant-upload-drag-icon'>
          <InboxOutlined />
        </p>
        <p className='ant-upload-text'>请拖拽.XLSX文件至此或使用点击上传</p>
        <p className='ant-upload-hint'>
          支持单次或批量上传。 严禁上传公司资料或其他公司文件
        </p>
      </Dragger>
    </div>
  );
  return (
    <>
      <Config
        setName={setName}
        setType={setType}
        setAddress={setAddress}
        setPage={setPage}
        typeInfo={TypeInfo}
      ></Config>
      <Divider orientation='left'></Divider>
      <h2 style={{ fontWeight: "bold", display: "inline-block" }}>详细信息</h2>
      {
        window.localStorage.getItem('identity') === null ?
          null
          :
          <Space style={{ float: "right" }}>
            <Dropdown
              trigger={["click"]}
              placement='bottomLeft'
              arrow
              overlay={menu}
            >
              <Button type='primary'>批量导入</Button>
            </Dropdown>

            <Button
              type='primary'
              onClick={() => {
                setContactEmail("");
                setContactPerson("");
                setContactTel("");
                setCreatTime("");
                setInvest([]);
                setDescription("");
                setFinancialName("");
                setFinancialType("");
                setFinancialAddress("");
                setUpdateTime(moment().format("YYYY-MM-DD HH:mm:ss"));
                setTrue();
              }}
            >
              添加金融机构
            </Button>
          </Space>
      }

      <Table
        rowSelection={rowSelection}
        dataSource={dataSource}
        size='middle'
        footer={() => {
          return (
            <>
              {
                window.localStorage.getItem('identity') === null ?
                  null
                  :
                  <Space>
                    <Button
                      onClick={() => {
                        if (key?.length !== 0) {
                          const data: string[][] = [
                            [
                              "机构名称",
                              "组织结构代码",
                              "机构类型",
                              "地址",
                              "成立时间",
                              "投资方向",
                              "联系人",
                              "电话",
                              "Email",
                              "机构简介",
                            ],
                          ];

                          key?.forEach((value: string) => {
                            const temdata = dataSource.filter(v => {
                              if (v.id === parseInt(value)) return true;
                              return false;
                            });
                            data.push([
                              temdata[0].name,
                              temdata[0].code,
                              temdata[0].typeName,
                              temdata[0].address,
                              temdata[0].createTime,
                              temdata[0].invest.join(","),
                              temdata[0].contactPerson,
                              temdata[0].contactTel,
                              temdata[0].contactEmail,
                              temdata[0].description,
                            ]);
                          });

                          if (data.length !== 1) {
                            const blob = new Blob(
                              [xlsx.build([{ name: "Financial", data }])],
                              {
                                type: "application/octet-stream",
                              }
                            );
                            FileSaver.saveAs(blob, "FinancialOutput.xlsx");
                          }
                        }
                      }}
                    >
                      批量导出
                    </Button>
                    {key?.length === 0 ? (
                      <Button>批量删除</Button>
                    ) : (
                      <Popconfirm
                        title='确定删除这个机构吗?'
                        onConfirm={() => {
                          console.log("要删除的ID:", key);
                          deleteFinancial();
                        }}
                        okText='Yes'
                        cancelText='No'
                      >
                        <Button>批量删除</Button>
                      </Popconfirm>
                    )}
                  </Space>
              }
            </>
          );
        }}
        pagination={{
          current: page,
          showSizeChanger: false,
          pageSize: 10,
          total: count,
          onChange: (page, _pageSize) => {
            setPage(page);
          },
        }}
      >
        <Column title='机构名称' dataIndex='name' key='name' />
        <Column title='机构类型' dataIndex='typeName' key='typeName' />
        <Column title='投资方向' dataIndex='showinvest' key='showinvest' />
        <Column title='成立时间' dataIndex='createTime' key='createTime' />
        {
          window.localStorage.getItem('identity') === null ?
            null
            :
            <Column
              title='操作'
              dataIndex='operation'
              key='operation'
              render={(_text: string, record: PT.financialList) => (
                <Space size={10}>
                  {" "}
                  <Button
                    type={"link"}
                    onClick={() => {
                      const a = record.address.split(";").pop();
                      setFinancialInfo(record);
                      setFinancialName(record.name);
                      setFinancialCode(record.code);
                      a ? setFinancialAddress(a) : console.log(a);
                      setFinancialType(record.typeName);
                      setContactEmail(record.contactEmail);
                      setContactPerson(record.contactPerson);
                      setContactTel(record.contactTel);
                      setCreatTime(record.createTime);
                      setInvest(record.invest);
                      setDescription(record.description);
                      setId(record.id);
                      setShowDetail(true);
                      setUpdateTime(record.updateTime);
                      setFiller(record.filler);
                      console.log("详情", record);
                    }}
                  >
                    详情
                  </Button>
                  <Popconfirm
                    title='确定要删除这个机构吗?'
                    onConfirm={() => {
                      console.log("要删除的ID:", key);
                      deleteFinancial();
                    }}
                    okText='Yes'
                    cancelText='No'
                  >
                    <Button
                      type='link'
                      onClick={() => {
                        setKey([record.id.toString()]);
                      }}
                    >
                      删除
                    </Button>
                  </Popconfirm>
                </Space>
              )}
            />
        }
      </Table>
      <Modal
        destroyOnClose
        visible={isShow}
        title='添加金融机构信息'
        onOk={() => {
          setTrue();
        }}
        onCancel={() => {
          setFalse();
          setClick(false);
          setFinancialCode("");
        }}
        footer={null}
        width={"70vw"}
        style={{ top: 15 }}
      >
        <Form
          layout='horizontal'
          title='机构信息'
          onFinish={e => {
            creatfinancial(e);
            setFalse();
          }}
        >
          <Row justify='space-between'>
            <Col flex={1}>
              <Form.Item
                label=' 机构名称'
                name='name'
                labelAlign='right'
                rules={[
                  {
                    message: "机构名称不能为空！",
                    required: true,
                  },
                ]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setFinancialName(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='组织结构代码'
                name='code'
                labelAlign='right'
                help={
                  exist
                    ? "此组织结构代码已存在"
                    : isClick && financialCode === ""
                      ? "组织结构代码不能为空!"
                      : ""
                }
                validateStatus={
                  (isClick && financialCode === "") || exist
                    ? "error"
                    : "success"
                }
                rules={[
                  {
                    message: "组织结构代码不能为空！",
                    required: true,
                  },
                ]}
              >
                <Input
                  maxLength={50}
                  onClick={() => {
                    setClick(true);
                  }}
                  onChange={event => {
                    setFinancialCode(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row justify='start' gutter={[24, 0]}>
            <Col flex={1}>
              <Form.Item
                name='addressBefore'
                label='所在城市   '
                labelAlign='right'
                rules={[{ required: true, message: "城市不能为空" }]}
              >
                <Cascader
                  options={options}
                  placeholder='请输入机构所在的城市'
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='具体街道'
                name='address'
                labelAlign='right'
                rules={[{ required: true, message: "街道不能为空！" }]}
              >
                <Input
                  onChange={event => {
                    setFinancialAddress(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>

            <Col flex={1}>
              <Form.Item
                label='机构类型'
                labelAlign='right'
                name='type'
                rules={[{ required: true, message: "机构类型不能为空！" }]}
              >
                <Select
                  defaultValue=''
                  style={{ width: 120 }}
                  allowClear
                  onChange={value => {
                    setFinancialType(value);
                  }}
                >
                  {TypeInfo?.map(type => (
                    <Option value={type.name}>{type.name}</Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row justify='start' gutter={[24, 0]}>
            <Col flex={1}>
              <Form.Item
                label='投资方向'
                labelAlign='right'
                name='invest'
                rules={[{ required: true, message: "投资方向不能为空！" }]}
              >
                <Select
                  mode='multiple'
                  style={{ width: "100%" }}
                  defaultValue={[]}
                  onChange={value => {
                    setInvest(value);
                  }}
                >
                  {investInfo?.map(type => (
                    <Option value={type}>{type}</Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='成立时间'
                labelAlign='right'
                name='createTime'
                rules={[{ required: true, message: "成立时间不能为空！" }]}
              >
                <DatePicker
                  onChange={(date, dateString) => {
                    setCreatTime(dateString);
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row justify='space-between'>
            <Col flex={1} span={6}>
              <Form.Item
                label='联系人'
                labelAlign='right'
                name='prople'
                rules={[{ required: true, message: "联系人不能为空！" }]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setContactPerson(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>
            <Col flex={1} span={6}>
              <Form.Item
                label='电话'
                labelAlign='right'
                name='tel'
                rules={[{ required: true, message: "电话不能为空！" }]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setContactTel(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>
            <Col flex={1} span={6}>
              <Form.Item
                label='E-Mail'
                labelAlign='right'
                name='E-mail'
                rules={[{ required: true, message: "E-mail不能为空！" }]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setContactEmail(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            label='机构简介'
            labelAlign='right'
            name='discription'
            rules={[{ required: true, message: "机构简介不能为空！" }]}
          >
            <TextArea
              style={{ resize: "none" }}
              maxLength={100}
              placeholder='机构简介（限制在100字之内）'
              onChange={event => {
                setDescription(event.target.value);
              }}
            />
          </Form.Item>
          <Form.Item label='附件材料' labelAlign='right'>
            <Upload
              name='file'
              accept='.pdf'
              maxCount={1}
              onRemove={file => {
                const index = fileList.indexOf(file);
                const newFileList = fileList.slice();
                newFileList.splice(index, 1);
                setFileList(newFileList);
              }}
              beforeUpload={file => {
                console.log(file);
                setFileList([file]);
                return false;
              }}
              fileList={fileList}
            >
              <Button type={"primary"} icon={<UploadOutlined />}>
                添加附件(PDF格式)
              </Button>
            </Upload>
          </Form.Item>
          <Row justify='start'>
            <Col span={6} flex={1}>
              <Form.Item label='录入时间' labelAlign='right' name='updateTime'>
                <DatePicker
                  value={moment()}
                  defaultValue={moment()}
                  onChange={(date, dateString) => {
                    setUpdateTime(dateString);
                  }}
                  disabled
                />
              </Form.Item>
            </Col>
            <Col span={6} flex={1}>
              <Form.Item label='填写人' labelAlign='right' name='write'>
                <Input
                  maxLength={50}
                  defaultValue={filler}
                  value={filler}
                  readOnly
                ></Input>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item>
            <Space style={{ float: "right" }}>
              <Button
                loading={loading}
                type='primary'
                htmlType='submit'
                onClick={() => {
                  setClick(true);
                }}
              >
                提交
              </Button>
              <Button
                key='back'
                onClick={() => {
                  setFalse();
                  setClick(false);
                  setFinancialCode("");
                }}
              >
                取消
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        visible={showDetail}
        title='查看金融机构信息'
        onOk={() => setShowDetail(true)}
        onCancel={() => {
          setShowDetail(false);
          setClick(false);
          setFinancialCode("");
        }}
        destroyOnClose
        footer={null}
        width={"70vw"}
        style={{ top: 15 }}
      >
        <Form
          layout='horizontal'
          title='机构信息'
          initialValues={{
            name: financialInfo?.name,
            address: add,
            addressBefore: before,
            type: financialInfo?.organization_type.name,
            discription: financialInfo?.description,
            Email: financialInfo?.contactEmail,
            tel: financialInfo?.contactTel,
            prople: financialInfo?.contactPerson,
            invest: financialInfo?.invest,
            createTime: moment(financialInfo?.createTime, "YYYY/MM/DD"),
            write: filler,
            updateTime: moment(financialInfo?.updateTime, "YYYY/MM/DD"),
          }}
          onFinish={e => {
            updatefinancial(e);
            setShowDetail(false);
          }}
        >
          <Row justify='space-between'>
            <Col flex={1}>
              <Form.Item
                label=' 机构名称'
                name='name'
                labelAlign='right'
                rules={[{ required: true }]}
              >
                <Input
                  maxLength={50}
                  defaultValue={financialInfo?.name}
                  onChange={event => {
                    setFinancialName(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item label=' 组织结构代码' name='code' labelAlign='right'>
                <Input
                  maxLength={50}
                  defaultValue={financialInfo?.code}
                  onChange={event => { }}
                  disabled
                />
              </Form.Item>
            </Col>
          </Row>

          <Row justify='start' gutter={[24, 0]}>
            <Col flex={1}>
              <Form.Item
                name='addressBefore'
                label='选择城市   '
                labelAlign='right'
                rules={[{ required: true, message: "城市不能为空" }]}
              >
                <Cascader
                  options={options}
                  placeholder='请输入机构所在的城市'
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='具体街道'
                labelAlign='right'
                name='address'
                rules={[{ required: true, message: "街道不能为空！" }]}
              >
                <Input
                  defaultValue={financialInfo?.address}
                  onChange={event => {
                    //console.log(event.target.value);
                    setFinancialAddress(event.target.value);
                  }}
                />
              </Form.Item>
            </Col>

            <Col flex={1}>
              <Form.Item
                label='机构类型'
                labelAlign='right'
                name='type'
                rules={[{ required: true, message: "机构类型不能为空！" }]}
              >
                <Select
                  defaultValue={financialInfo?.organization_type.name}
                  style={{ width: 120 }}
                  allowClear
                  onChange={value => {
                    setFinancialType(value);
                  }}
                >
                  {TypeInfo?.map(type => (
                    <Option value={type.name}>{type.name}</Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row justify='space-between' gutter={[24, 0]}>
            <Col flex={1}>
              <Form.Item
                label='成立时间'
                labelAlign='right'
                name='createTime'
                rules={[{ required: true, message: "成立时间不能为空！" }]}
              >
                <DatePicker
                  value={moment(financialInfo?.createTime, "YYYY/MM/DD")}
                  defaultValue={moment(financialInfo?.createTime, "YYYY/MM/DD")}
                  onChange={(date, dateString) => {
                    setCreatTime(dateString);
                  }}
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='投资方向'
                labelAlign='right'
                name='invest'
                rules={[{ required: true, message: "投资方向不能为空！" }]}
              >
                <Select
                  mode='multiple'
                  style={{ width: "100%" }}
                  defaultValue={financialInfo?.invest}
                  onChange={value => {
                    setInvest(value);
                  }}
                >
                  {investInfo?.map(type => (
                    <Option value={type}>{type}</Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row justify='space-between'>
            <Col flex={1}>
              <Form.Item
                label='联系人'
                labelAlign='right'
                name='prople'
                rules={[{ required: true, message: "联系人不能为空！" }]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setContactPerson(event.target.value);
                  }}
                  defaultValue={financialInfo?.contactPerson}
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='电话'
                labelAlign='right'
                labelCol={{ span: 4 }}
                name='tel'
                rules={[{ required: true, message: "电话不能为空！" }]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setContactTel(event.target.value);
                  }}
                  defaultValue={financialInfo?.contactTel}
                />
              </Form.Item>
            </Col>
            <Col flex={1}>
              <Form.Item
                label='E-Mail'
                labelAlign='right'
                labelCol={{ span: 4 }}
                name='Email'
                rules={[{ required: true, message: "E-mail不能为空！" }]}
              >
                <Input
                  maxLength={50}
                  onChange={event => {
                    setContactEmail(event.target.value);
                  }}
                  defaultValue={financialInfo?.contactEmail}
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            label='机构简介'
            labelAlign='right'
            name='discription'
            rules={[{ required: true, message: "机构简介不能为空！" }]}
          >
            <TextArea
              style={{ resize: "none" }}
              maxLength={100}
              defaultValue={financialInfo?.description}
              placeholder='机构简介（限制在100字之内）'
              onChange={event => {
                setDescription(event.target.value);
              }}
            />
          </Form.Item>
          <Form.Item label='附件材料' labelAlign='right'>
            <Upload
              name='file'
              accept='.pdf'
              maxCount={1}
              action={UPLOAD_ORGANIZATION_FILE}
              data={{ id: financialInfo?.id }}
              headers={{
                Authorization: localStorage.getItem("Authorization") ?? "",
              }}
              onChange={info => {
                if (info.file.status !== "uploading") {
                  setLoading(true);
                  console.log(info.file, info.fileList);
                }
                if (info.file.status === "done") {
                  setLoading(false);

                  message.success(`${info.file.name} 文件上传成功`);
                } else if (info.file.status === "error") {
                  setLoading(false);

                  message.error(`${info.file.name} 文件上传失败请重试.`);
                }
              }}
            >
              <Button
                type={"primary"}
                loading={loading}
                icon={<UploadOutlined />}
              >
                添加附件(PDF格式)
              </Button>
            </Upload>
          </Form.Item>
          <Form.Item label='附件材料' labelAlign='right'>
            <Button
              onClick={() => {
                setLoading(true);
                axios
                  .post(FINANCIAL_FILE_EXIST, { id: financialInfo?.id })
                  .then(r => {
                    const { msg } = r.data;
                    message.success(msg);
                    window.location.href =
                      GET_ORGANIZATION_FILE + `?id=${financialInfo?.id}`;

                    setLoading(false);
                  })
                  .catch(e => {
                    setLoading(false);
                  });
              }}
              type={"primary"}
              loading={loading}
              icon={<CloudDownloadOutlined />}
            >
              下载已上传附件
            </Button>
          </Form.Item>
          <Row justify='start'>
            <Col span={6} flex={1}>
              <Form.Item label='录入时间' labelAlign='right' name='updateTime'>
                <DatePicker
                  disabled
                  value={moment(financialInfo?.updateTime, "YYYY/MM/DD")}
                  defaultValue={moment(financialInfo?.updateTime, "YYYY/MM/DD")}
                  onChange={(date, dateString) => {
                    setUpdateTime(dateString);
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={6} flex={1}>
              <Form.Item label='填写人' labelAlign='right' name='write'>
                <Input
                  maxLength={50}
                  defaultValue={filler}
                  value={filler}
                  readOnly
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item>
            <Space style={{ float: "right" }}>
              <Button loading={loading} type='primary' htmlType='submit'>
                提交
              </Button>
              <Button
                key='back'
                onClick={() => {
                  setShowDetail(false);
                  setClick(false);
                  setFinancialCode("");
                }}
              >
                取消
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
