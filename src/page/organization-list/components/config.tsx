import { Input, Typography, Select, Form } from "antd";
import React from "react";
interface configProps {
  setName: Function;
  setAddress: Function;
  setType: Function;
  setPage: Function;
  typeInfo: { name: string; uuid: string }[];
}
const { Option } = Select;
const { Title } = Typography;
export default (props: configProps) => {
  const { typeInfo, setName, setAddress, setType, setPage } = props;

  return (
    <div>
      <Title level={4}>筛选查询</Title>
      <Form layout="inline">
        <Form.Item>
          <Input
            placeholder="请输入机构名称"
            onChange={(event) => {
              setName(event.target.value);
              setPage(1);
            }}
            // enterButton
          />
        </Form.Item>
        <Form.Item label=" 机构类型">
          <Select
            defaultValue=""
            style={{ width: 120 }}
            allowClear
            onChange={(value) => {
              setType(value);
            }}
          >
            <Option value="">全部</Option>
            {typeInfo?.map((type) => (
              <Option value={type.uuid}>{type.name}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Input
            addonBefore=" 机构地址："
            onChange={(event) => {
              setAddress(event.target.value);
            }}
          />
        </Form.Item>
      </Form>
    </div>
  );
};
