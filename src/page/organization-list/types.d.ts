declare namespace PT {
  /**
   * 表格数据
   */
  interface financialList {
    typeName: string;
    key: number;
    invest: string[];
    showinvest: string[];
    id: number;
    uuid: string;
    name: string;
    address: string;
    fk_type: string;
    filler: string;
    createTime: string;
    description: string;
    contactTel: string;
    contactPerson: string;
    contactEmail: string;
    updateTime: string;
    createdAt: string;
    updatedAt: string;
    code: string;
    investment_directions: direction[];
    organization_type: {
      name: string;
    };
  }

  interface direction {
    id: number;
    uuid: string;
    name: string;
    createdAt: string;
    updatedAt: string;
    merge_organization_investment: {
      id: number;
      organization_id: number;
      investment_id: number;
      createdAt: string;
      updatedAt: string;
    };
  }
}
