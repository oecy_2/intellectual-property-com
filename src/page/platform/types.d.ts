declare namespace IP {
  /**
   * 表格数据
   */
  interface Platform {
    id?: number;
    uuid?: number;
    contactPerson: string; // 平台联系人姓名
    contactEmail: string; // 平台联系人email
    contactTel: string; // 联系人电话
    description: string; // 服务平台介绍
  }
}
