import React, { lazy } from 'react';

const PlatformComponents = lazy(() => import('./components/index'));


// 整体组件逻辑,lazy等等.
export default () => <PlatformComponents />;
