import React, { useEffect, useState } from 'react'
import {  Input, Card, Form} from 'antd'
import Show from './show'
import axios from 'axios'
import * as APIS from '../../../constants/api-constants'
const { TextArea } = Input

const PlatformComponents = () => {
  const [platformData, setPlatformData] = useState<IP.Platform>()
  useEffect(() => {
    axios
      .get(APIS.PLATFORM)
      .then((r) => {
        const { mainData } = r.data

        console.log('mainData:', mainData)
        setPlatformData(mainData.dataSource)
      })
      .catch(() => {
        alert('服务器异常')
      })
  }, [])
  return (
    <>
     <Form layout='inline'>
        <Form.Item>
            <Input
          size="large"
          placeholder="平台客服经理"
          value={platformData?.contactPerson}
          addonBefore={'联系人姓名：'}
          disabled
        />
        </Form.Item>
        <Form.Item >
        <Input
          size="large"
          placeholder="电话"
          value={platformData?.contactTel}
          addonBefore={'电话：'}
          disabled
        />
        </Form.Item>
        <Form.Item>
        <Input
          size="large"
          placeholder="sercive@hotmail.com"
          value={platformData?.contactEmail}
          addonBefore={'E-Mail：'}
          disabled
        />
        </Form.Item>
      </Form>
    
      <Card title="知识产权综合服务平台介绍">
        <TextArea value={platformData?.description} autoSize={true} />
        <Show />
      </Card>
    </>
  )
}

export default PlatformComponents
