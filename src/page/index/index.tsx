import React, {useCallback, useState} from 'react';

import Login from './login/index';
import Registry from './registry/index';
import './style.css';

/** index页处于登录状态 */
export const LOGIN = 'login';
/** index页处于注册状态 */
export const REGISTER = 'register';

/**
 * show
 */
const Show = () => {
    const [pageState, setPageState] = useState(LOGIN);
    const onPageStateChange = useCallback<(pageState: string) => void>(
        pageState => {
            setPageState(pageState);
        },
        []
    );

    return (
        <div>
            <div>
                {pageState === LOGIN ? (
                    <Login onPageStateChange={onPageStateChange}/>
                ) : pageState === REGISTER ? <Registry onPageStateChange={onPageStateChange}/> : null}
            </div>
        </div>
    );
};

export default Show;
