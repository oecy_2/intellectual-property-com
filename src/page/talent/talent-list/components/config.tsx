import { DatePicker, Form, Input, Select, Typography } from "antd";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { GLOBAL_SELECTIONS } from "src/constants/api-constants";

const { Title } = Typography;
const { Option } = Select;

interface configProps {
  setDate: Function;
  setOrganization: Function;
  setPage: Function;
  setTalentType: Function;
  setName: Function;
}
export default (props: configProps) => {
  const { setName, setTalentType, setOrganization, setDate } = props;
  const [talentTypeOptions, setTalentTypeOptions] = useState<any>([]);

  useEffect(() => {
    axios
      .get(GLOBAL_SELECTIONS)
      .then((r) => {
        const { mainData } = r.data;
        const talentTypeOptions = [
          <Option key={0} value={""}>
            全部
          </Option>,
          ...mainData.talentType.map((item: any) => {
            return (
              <Option key={item.uuid} value={item.typeName}>
                {item.typeName}
              </Option>
            );
          }),
        ];

        setTalentTypeOptions(talentTypeOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);
  return (
    <div>
      <div>
        <Title level={4}>筛选查询</Title>
      </div>
      <Form layout="inline">
        <Form.Item name="name">
          <Input
            placeholder={"请输入姓名"}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </Form.Item>
        <Form.Item name="talentType" label="人才类型">
          <Select
            style={{ width: 100 }}
            defaultValue=""
            onSelect={(e) => {
              console.log(e);
              setTalentType(e);
            }}
          >
            {talentTypeOptions}
          </Select>
        </Form.Item>
        <Form.Item name="organization" label="工作单位">
          <Input
            placeholder={"请输入工作单位"}
            onChange={(e) => {
              setOrganization(e.target.value);
            }}
          />
        </Form.Item>
        <Form.Item name="date">
          <DatePicker
            onChange={(e) => {
              if (e) {
                const date = e.toDate();

                setDate(
                  `${date.getFullYear()}-${
                    date.getUTCMonth() + 1 > 10
                      ? date.getUTCMonth() + 1
                      : "0" + (date.getUTCMonth() + 1)
                  }-${
                    date.getUTCDate() > 10
                      ? date.getUTCDate()
                      : "0" + date.getUTCDate()
                  }`
                );
              } else {
                setDate("");
              }
            }}
          />
        </Form.Item>
      </Form>
    </div>
  );
};
