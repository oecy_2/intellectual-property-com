import React, { useEffect, useState } from "react";
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  message,
  Space,
  Upload,
  Cascader,
} from "antd";
import axios from "axios";
import FileSaver from "file-saver";
import {
  CloudDownloadOutlined,
  UploadOutlined,
} from "@ant-design/icons/lib/icons";
import {
  GLOBAL_SELECTIONS,
  TALENT_UPDATE,
  GET_TALENT_ATTACHMENT,
  TALENT_FILE_EXIST,
} from "src/constants/api-constants";
import provice from "../../../../util/place";

const { Option } = Select;
const options = provice;
interface DetailProps {
  setDetailFalse: Function;
  record?: TalentList.item;
  getList: Function;
  setPage: Function;
}
const Detail: React.FC<DetailProps> = (props) => {
  const { setDetailFalse, record, getList } = props;
  const [educationOptions, setEducationOptions] = useState([]);
  const [applicationAreaOptions, setApplicationAreaOptions] = useState([]);
  const [talentTypeOptions, setTalentTypeOptions] = useState([]);
  const [organizationTypeOptions, setOrganizationTypeOptions] = useState<any>(
    []
  );
  const [titleTypeOptions, setTitleTypeOptions] = useState<any>([]);
  const [positionTypeOptions, setPositionTypeOptions] = useState<any>([]);
  const [loading, setLoading] = useState(false);

  console.log(record);

  const formLayout = {
    labelCol: {
      span: 9,
    },
    wrapperCol: {
      span: 12,
    },
  };
  const formItemLayout1 = {
    labelCol: {
      span: 3,
    },
    wrapperCol: {
      span: 20,
    },
  };

  const formItemLayout2 = {
    labelCol: {
      span: 3,
    },
    wrapperCol: {
      span: 21,
    },
  };

  useEffect(() => {
    axios
      .get(GLOBAL_SELECTIONS)
      .then((r) => {
        const { mainData } = r.data;

        const applicationAreaOptions = mainData.applicationArea.map(
          (item: any) => {
            return (
              <Option key={item.uuid} value={item.uuid}>
                {item.name}
              </Option>
            );
          }
        );

        const organizationsTypeOptions = mainData.organizationType.map(
          (item: any) => {
            return (
              <Option key={item.uuid} value={item.uuid}>
                {item.name}
              </Option>
            );
          }
        );

        const talentTypeOptions = mainData.talentType.map((item: any) => {
          return (
            <Option key={item.uuid} value={item.uuid}>
              {item.typeName}
            </Option>
          );
        });

        const educationOptions = mainData.education.map((item: any) => {
          return (
            <Option key={item} value={item}>
              {item}
            </Option>
          );
        });

        const titleOptions = mainData.titleType.map((item: any) => {
          return (
            <Option key={item.uuid} value={item.uuid}>
              {item.name}
            </Option>
          );
        });

        const positionOptions = mainData.positionType.map((item: any) => {
          return (
            <Option key={item.uuid} value={item.uuid}>
              {item.name}
            </Option>
          );
        });

        setApplicationAreaOptions(applicationAreaOptions);
        setOrganizationTypeOptions(organizationsTypeOptions);
        setTalentTypeOptions(talentTypeOptions);
        setEducationOptions(educationOptions);
        setTitleTypeOptions(titleOptions);
        setPositionTypeOptions(positionOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [record]);

  
  return (
    <Form
      layout="horizontal"
      labelAlign="right"
      {...formLayout}
      colon={true}
      initialValues={record}
      onFinish={(e) => {
        const formData = new FormData();
        const addressBefore = e.addressBefore.join(";") + ";";
        formData.append("uuid", record?.uuid ? record?.uuid : "");
        formData.append("applicationArea", e.applicationArea);
        formData.append("description", e.description);
        formData.append("education", e.education);
        formData.append("address", addressBefore + e.address);
        formData.append("email", e.email);
        formData.append("name", e.name);
        formData.append("position", e.position);
        formData.append("title", e.title);
        formData.append("type", e.type);
        formData.append("unit", e.unit);
        formData.append("sex", e.sex);
        formData.append("age", e.age);
        formData.append("talentType", e.talentType);
        formData.append("tel", e.tel);
        formData.append(
          "file",
          e.file ? e.file.file : new File([], "null.pdf")
        );

        axios
          .post(TALENT_UPDATE, formData)
          .then((r) => {
            console.log(r);
            if (r.data.mainData.upload === true) {
              message.info("提交成功");
            } else {
              message.error("提交失败");
            }
          })
          .catch((e) => {
            message.error("提交失败");
            console.log(e);
          })
          .finally(() => {
            setDetailFalse();
            getList();
          });
      }}
    >
      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            label="姓名"
            name="name"
            rules={[{ required: true, message: "姓名不能为空" }]}
          >
            <Input></Input>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="性别"
            name="sex"
            rules={[{ required: true, message: "性别不能为空" }]}
          >
            <Select>
              <Option value={1}>男</Option>
              <Option value={0}>女</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="年龄"
            name="age"
            rules={[{ required: true, message: "年龄不能为空" }]}
          >
            <Input type="number" max={100} min={0} step={1}></Input>
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={18}>
          <Form.Item
            label="工作单位"
            name="unit"
            {...formItemLayout1}
            rules={[{ required: true, message: "工作单位不能为空" }]}
          >
            <Input></Input>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="单位性质"
            rules={[{ required: true, message: "单位性质不能为空" }]}
            name="type"
          >
            <Select>{organizationTypeOptions}</Select>
          </Form.Item>
        </Col>
      </Row>

      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            label="职称"
            name="title"
            rules={[{ required: true, message: "职称不能为空" }]}
          >
            <Select>{titleTypeOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="职务"
            name="position"
            rules={[{ required: true, message: "岗位不能为空" }]}
          >
            <Select>{positionTypeOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="行业领域"
            name="applicationArea"
            rules={[{ required: true, message: "行业领域不能为空" }]}
          >
            <Select>{applicationAreaOptions}</Select>
          </Form.Item>
        </Col>
      </Row>
      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            label="人才类型"
            name="talentType"
            rules={[{ required: true, message: "人才类型不能为空" }]}
          >
            <Select>{talentTypeOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="学历"
            name="education"
            rules={[{ required: true, message: "学历不能为空" }]}
          >
            <Select>{educationOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="电话"
            name="tel"
            /* rules={[
              {
                validator: (_, value) => {
                  if (value.length === 0 || !value) {
                    return Promise.reject(new Error("电话不能为空"));
                  }
                  const pat = /^(((\(\d{3,4}\)|\d{3,4})?\d{7,8})|(1[3-9][0-9]{9}))$/;

                  if (!pat.test(value)) {
                    return Promise.reject(new Error("电话格式不正确"));
                  }

                  if (
                    value.length === 7 ||
                    value.length === 8 ||
                    value.length === 11
                  ) {
                    return axios.post(TEL_EXIST, { tel: value }).then((res) => {
                      console.log(value);

                      console.log(res);
                      if (res.data.mainData) {
                        return Promise.reject(new Error("电话号码已被注册"));
                      }
                      return Promise.resolve();
                    });
                  }
                },
              },
            ]} */
          >
            <Input disabled></Input>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label="E-Mail"
            name="email"
            rules={[
              {
                type: "email",
                message: "邮箱格式不正确",
              },
              { required: true, message: "email不能为空" },
            ]}
          >
            <Input></Input>
          </Form.Item>
        </Col>
      </Row>
      <Row justify="space-between">
        <Col span={6}>
          <Form.Item
            name="addressBefore"
            label="居住城市"
            rules={[{ required: true, message: "城市不能为空" }]}
          >
            <Cascader options={options} />
          </Form.Item>
        </Col>
        <Col span={18}>
          <Form.Item
            label="具体街道"
            name="address"
            rules={[{ required: true, message: "街道不能为空！" }]}
          >
           <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: "100%" }}>
        <Col span={18}>
          <Form.Item
            name="description"
            label="人才简历"
            {...formItemLayout2}
            rules={[{ required: true, message: "人才简历不能为空" }]}
          >
            <Input.TextArea
              maxLength={500}
              rows={4}
              style={{ resize: "none" }}
            ></Input.TextArea>
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: "100%" }}>
        <Col span={18}>
          <Form.Item
            name="file"
            label="附件材料"
            labelAlign="right"
            {...formItemLayout1}
          >
            <Upload
              name="logo"
              beforeUpload={() => {
                return false;
              }}
              maxCount={1}
              accept={".pdf"}
            >
              <Button icon={<UploadOutlined />} type="primary">
                添加附件(PDF格式)
              </Button>
            </Upload>
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: "100%" }}>
        <Col span={18}>
          <Form.Item label="附件材料" labelAlign="right" {...formItemLayout1}>
            <Button
              loading={loading}
              onClick={() => {
                setLoading(true);
                axios
                  .post(TALENT_FILE_EXIST, { id: record?.id })
                  .then((r) => {
                    const { msg, mainData } = r.data;
                    message.success(msg);
                    FileSaver.saveAs(
                      GET_TALENT_ATTACHMENT + `?id=${record?.id}`,
                      mainData?.name
                    );
                    setLoading(false);
                  })
                  .catch((e) => {
                    message.error('该人才未上传简历');
                    setLoading(false);
                  });
              }}
              icon={<CloudDownloadOutlined />}
              type="primary"
            >
              下载已有附件
            </Button>
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: "100%", marginTop: "10vh" }} align="bottom">
        <Col span={6}>
          <Form.Item label="录入时间" name="updateTime">
            <Input disabled></Input>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="填写人" name="filler">
            <Input readOnly></Input>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item wrapperCol={{ span: 22 }}>
            <Space style={{ float: "right" }}>
              <Button type="primary" htmlType="submit">
                保存
              </Button>
              <Button
                type="primary"
                onClick={() => {
                  setDetailFalse();
                }}
              >
                返回
              </Button>
            </Space>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default Detail;
