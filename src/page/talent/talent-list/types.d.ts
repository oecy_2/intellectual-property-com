declare namespace TalentList {
  interface item {
    id: string;
    uuid: string;
    name: string;
    sex: string;
    tel: string;
    email: string;
    age: string;
    title: string;
    titleName: string;
    education: string;
    applicationAreaName: string;
    application_area: any;
    unit: string;
    type: string;
    organization: any;
    talentType: string;
    talentTypeName: string;
    talent_classfication: any;
    filler: string;
    description: string;
    position: string;
    positionName: string;
    updateTime: string;
    address: string;
    addressBefore: string;
    educationName: string;
    organization_type: any;
  }
}
