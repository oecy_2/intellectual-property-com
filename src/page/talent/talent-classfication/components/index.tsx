import {
  Button,
  Modal,
  Popconfirm,
  Form,
  Table,
  Divider,
  Row,
  Space,
  Input,
  message,
} from "antd";
import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  GET_TALENT_TYPE_LIST,
  TALENT_TYPE_DEL,
  TALENT_TYPE_INSERT,
  TALENT_TYPE_HAVE_TALENT,
} from "src/constants/api-constants";
import { useBoolean } from "ahooks";

export default () => {
  const [dataSource, setDataSource] = useState<ProjectList.item[]>();
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [
    isDetailShow,
    { setTrue: setDetailShow, setFalse: setDetailFalse },
  ] = useBoolean(false);
  const [
    submiting,
    { setTrue: setSubmiting, setFalse: setSubmitingFalse },
  ] = useBoolean(false);
  const [form] = Form.useForm();
  const TITLE_STATE = {
    TRUE: "将删除该分类下所有人才及其关联关系，确定要删除么",
    FALSE: "一但删除不可恢复，确定要删除么",
  };
  const [title, setTitle] = useState(TITLE_STATE.FALSE);

  const getList = () => {
    setLoading(true);
    axios
      .post(GET_TALENT_TYPE_LIST, {
        page,
      })
      .then((r) => {
        const { mainData } = r.data;
        setCount(mainData.count);
        const dataSource = mainData.rows.map((item: any, index: number) => {
          return {
            ...item,
            key: index,
          };
        });
        setDataSource(dataSource);
      })
      .catch((e) => {
        console.log(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(getList, []);

  let columns = [
    {
      title: "id",
      dataIndex: "id",
      key: "id",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "分类名称",
      dataIndex: "typeName",
      key: "typeName",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "分类说明",
      dataIndex: "typeDescription",
      key: "typeDescription",
      render: (text: string) => {
        return (
          <span
            style={{
              wordBreak: "break-all",
              width: "auto",
              display: "block",
              whiteSpace: "pre-wrap",
            }}
          >
            {text}
          </span>
        );
      },
    },
    {
      title: "操作",
      render: (_value: any, record: any) => {
        return (
          <>
            <Popconfirm
              title={title}
              onConfirm={() => {
                axios
                  .post(TALENT_TYPE_DEL, {
                    uuid: record.uuid,
                  })
                  .then(() => {
                    setPage(1);
                    getList();
                  })
                  .catch(() => {
                    setPage(1);
                    getList();
                  });
              }}
              okText="确定"
              cancelText="取消"
            >
              <Button
                onClick={() => {
                  axios
                    .post(TALENT_TYPE_HAVE_TALENT, { uuid: record.uuid })
                    .then((r) => {
                      const haveTalents = r.data.mainData;
                      console.log("haveTalents", haveTalents);
                      setTitle(
                        haveTalents ? TITLE_STATE.TRUE : TITLE_STATE.FALSE
                      );
                    })
                    .catch((e) => {
                      message.error("系统错误");
                    });
                }}
                type={"link"}
              >
                删除
              </Button>
            </Popconfirm>
          </>
        );
      },
    },
  ];

  if(window.localStorage.getItem('identity') === null){
    columns = columns.filter(column => {
     return  column.title !== "操作";
    });
  }

  return (
    <>
      <Row justify="space-between">
        <h2 style={{ fontWeight: "bold", display: "inline-block" }}>
          人才分类
        </h2>
        <Button
          type="primary"
          onClick={() => {
            setDetailShow();
          }}
        >
          新增人才分类
        </Button>
      </Row>

      <Divider orientation="left"></Divider>
      <Table
        pagination={{
          current: page,
          showSizeChanger: false,
          pageSize: 10,
          total: count,
          onChange: (page, _pageSize) => {
            setLoading(true);
            setPage(page);
            setLoading(false);
          },
        }}
        bordered={true}
        loading={loading}
        size={"middle"}
        columns={columns}
        dataSource={dataSource}
      />

      <Modal
        title="新增人才分类"
        visible={isDetailShow}
        onCancel={setDetailFalse}
        width={"30vw"}
        centered={true}
        footer={null}
        destroyOnClose
      >
        <Form
          layout="vertical"
          form={form}
          onFinish={(e) => {
            setSubmiting();
            axios
              .post(TALENT_TYPE_INSERT, {
                typeName: e.typeName,
                typeDescription: e.typeDescription,
              })
              .then((data) => {
                console.log(data);
                getList();
              })
              .catch((e) => {
                console.log(e);
              })
              .finally(() => {
                form.resetFields();
                setSubmitingFalse();
                getList();
              });
          }}
        >
          <Form.Item
            label="分类名称"
            name="typeName"
            rules={[{ required: true, message: "请输入分类名称" }]}
          >
            <Input.TextArea
              placeholder="请输入分类名称"
              rows={2}
              style={{ resize: "none" }}
            ></Input.TextArea>
          </Form.Item>
          <Form.Item label="分类描述" name="typeDescription">
            <Input.TextArea
              placeholder="请输入分类描述"
              rows={4}
              style={{ resize: "none" }}
            ></Input.TextArea>
          </Form.Item>
          <Form.Item>
            <Space>
              <Button htmlType="submit">完成并添加下一项</Button>
              <Button
                type="primary"
                loading={submiting}
                onClick={() => {
                  setDetailFalse();
                }}
              >
                返回
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
