declare namespace TalentTypeList {
  interface item {
    key: number;
    id:string;
    uuid:string;
    typeName: string;
    typeDescription: string;
  }
}
