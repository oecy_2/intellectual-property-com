import React, { lazy } from 'react';

const Company = lazy(() => import('./component'));

// 整体组件逻辑,lazy等等.
export default () => <Company />;
