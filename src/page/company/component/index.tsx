import React, { useEffect, useState } from "react";
import { useBoolean } from "ahooks";
import Mock from 'mockjs';
import { v1 as uuid } from 'uuid';

import Config from "./config";
import Show from './show';
import BatchProcess from "./batch-process";

// FIXME: 模拟数据
const {companyList} = Mock.mock({
  'companyList|100': [
    {
      'id|+1': 1,
      'uuid|+1': uuid(),
      'name|+1': [
        '天津华银医学检验实验室有限公司',
        '天津洺乾雅盛医疗器械贸易有限公司',
        '天津博爱恩海科技开发有限公司',
        '天津润龙生物科技有限公司',
      ],
      'address': '天津市东丽区',
      'medicalField|1': ['医学检验', '经营', '其他'],
      createTime: '@date("yyyy-MM-dd HH:mm:ss")',
      'mainBusiness|+1': [
        '医疗器械的销售',
        '口腔医疗设备的研发',
        '医学检验',
        '冷链运输设备',
      ],
    },
  ],
})
export default ()=>{
  // const [companyList, setCompanyList] = useState<PT.Company[]>([]);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [isFresh, { setTrue }] = useBoolean(true);
  const [name, setName] = useState(''); // 筛选查询企业名称
  const [medicalField, setMedicalField] = useState(''); // 筛选医疗领域
  const [address, setAddress] = useState(''); // 筛选所在地

  useEffect(()=>{
    // TODO: 后端获取列表run
  }, [page, name, medicalField, address, isFresh])

  return (
    <>
      <Config 
        setName={setName} 
        setMedicalField={setMedicalField} 
        setAddress={setAddress}
      />
      <BatchProcess/>
      <Show 
        tableProps={companyList}
        setPage={setPage}
        handleRemove={setTrue}
        total={total}
        setTotal={setTotal}
      />
    </>
  );
}

