import { Form, Input } from "antd";
import Title from "antd/lib/typography/Title";
import React from "react";

/**
 * 筛选查询
 */

 interface ConfigProps {
  setName: Function;
  setMedicalField: Function;
  setAddress: Function;
}

export default ({
  setName, 
  setMedicalField, 
  setAddress
}: ConfigProps)=>{

  return (
    <>
    <div>
        <Title level={4}>筛选查询</Title>
      </div>
      <Form layout="inline">
        <Form.Item name="name">
          <Input
            placeholder={"请输入企业名称"}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </Form.Item>
        <Form.Item name="medicalField" label="医疗领域">
          <Input
            placeholder={"请输入医疗领域"}
            onChange={(e) => {
              setMedicalField(e.target.value);
            }}
          />
        </Form.Item>
        <Form.Item name="setAddress" label="所在地">
          <Input
            placeholder={"请输入企业所在地"}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
}
