import {
  Button,
  Space,
  Table,
  Divider,
  Popconfirm,
} from 'antd';
import Column from 'antd/lib/table/Column';
import React, { useState } from 'react';

import Detail from './detail';


interface Props {
  tableProps: PT.Company[];
  setPage: (page: number) => void;
  handleRemove: () => void;
  total: number;
  setTotal: (totle: number) => void;
}

export default ({
  tableProps, setPage, handleRemove, total, setTotal
}: Props) => {

  const [showDetail, setShowDetail] = useState(false);
  return (
    <>
      <Table
        size="middle"
        rowKey={(record) => record.uuid}
        dataSource={tableProps}
        pagination={{
          onChange: (page) => setPage(page),
          total,
        }}
      >
        <Column title="企业名称" dataIndex="name" key="name" />
        <Column title="医疗领域" dataIndex="medicalField" key="medicalField" />
        <Column title="成立日期" dataIndex="createTime" key="createTime" />
        <Column title="所在地" dataIndex="address" key="address" />
        {
          window.localStorage.getItem('identity') === null ?
            null
            :
            <Column
              width={350}
              title="操作"
              render={(record: PT.Company) => (
                <Space split={<Divider type="vertical" />}>
                  <Button
                    type="link"
                    size="small"
                    onClick={() => {
                      setShowDetail(true);
                    }}
                  >
                    修改
                  </Button>
                  <Popconfirm
                    title="删除后该问卷下的回答无法找回！是否删除?"
                    onConfirm={() => {
                      handleRemove();
                    }}
                    okText="确定"
                    cancelText="取消"
                  >
                    <Button type="link" size="small">
                      删除
                    </Button>
                  </Popconfirm>
                </Space>
              )}
            />
        }
      </Table>
      <Detail
        showDetail={showDetail}
        setShowDetail={setShowDetail}
      />
    </>
  );
};
