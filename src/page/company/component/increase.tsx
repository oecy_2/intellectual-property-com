import { UploadOutlined } from "@ant-design/icons";
import { 
  Col, 
  Form, 
  Input, 
  Row,
  Cascader,
  Upload,
  Button,
  Space,
  Modal,
  DatePicker,
  Select
} from "antd";
import React from "react";

import provice from '../../../util/place';
const { Option } = Select;
const options = provice;

interface IncreaseProps {
  showIncrease: boolean,
  setShowIncrease: (showIncrease: boolean)=>void
}
/**
 * 增加
 */
export default ({showIncrease,setShowIncrease }: IncreaseProps)=>{
  const [form] = Form.useForm();

  return (
    <>
    <Modal
      visible={showIncrease}
      title='添加企业'
      onCancel={() => {
        setShowIncrease(false);
        }}
      footer={null}
      width={"70vw"}
      style={{top: 15}}
    >
     <Form
        layout='horizontal'
        labelAlign='right'
        colon={true}
        form={form}
      >
      <Row justify='space-between'>
        <Col span={12}>
          <Form.Item
            label='企业名称'
            name='name'
            rules={[{ required: true, message: '企业名称不能为空' }]}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label='组织机构代码'
            name='companyNo'
            rules={[{ required: true, message: '组织结构代码不能为空' }]}
          >
            <Input placeholder='请输入组织机构代码'/>
          </Form.Item>
        </Col>
      </Row>
      <Row justify='space-between'>
        <Col span={12}>
          <Form.Item
            name='addressBefore'
            label='所在城市'
            rules={[{ required: true, message: '所在城市不能为空' }]}
          >
            <Cascader options={options} placeholder='请输入企业所在的城市' />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label='具体街道'
            name='address'
            rules={[{ required: true, message: '街道不能为空！' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row justify='space-between'>
        <Col span={12}>
            <Form.Item
              label='医疗领域'
              labelAlign='right'
              name='medicalField'
              rules={[{required: true, message: "医疗领域不能为空！"}]}
            >
              <Select
                mode='multiple'
                style={{width: "100%"}}
              >
                <Option key='0' value='体外诊断'>体外诊断</Option>
                <Option key='1' value='医疗设备'>医疗设备</Option>
                <Option key='2' value='医学检验'>医学检验</Option>
              </Select>
            </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label='成立时间'
            labelAlign='right'
            name='createTime'
            rules={[{required: true, message: "成立时间不能为空！"}]}
          >
              <DatePicker/>
          </Form.Item>
        </Col>
       
      </Row>
      <Row justify='space-between'>
        <Col span={24}>
          <Form.Item
              label='主营业务'
              name='mainBusiness'
              rules={[{ required: true, message: '主营业务不能为空' }]}
            >
              <Input />
            </Form.Item>
        </Col>   
      </Row>
      <Row style={{ width: '100%' }}>
        <Col span={24}>
          <Form.Item
            name='description'
            label='企业简介'
            rules={[{ required: true, message: '企业简介不能为空' }]}
          >
            <Input.TextArea
              maxLength={500}
              rows={4}
              style={{ resize: 'none' }}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: '100%' }}>
        <Col span={24}>
          <Form.Item label='附件材料' name='file'>
            <Upload>
              <Button type='primary' icon={<UploadOutlined />}>
                添加附件(PDF格式)
              </Button>
            </Upload>
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: '100%', marginTop: '10vh' }} align='bottom'>
        <Col span={6}>
          <Form.Item label='录入时间'>
            <Input disabled />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            label='填写人'
            name='filler'
            rules={[{ required: true, message: '填写人不能为空' }]}
            initialValue={localStorage.getItem('userName')}
          >
            <Input readOnly />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item wrapperCol={{ span: 22 }}>
            <Space style={{ float: 'right' }}>
              <Button
                type='primary'
                htmlType='submit'
              >
                确认
              </Button>
              <Button
                type='primary'
                onClick={()=>{setShowIncrease(false)}}
              >
                返回
              </Button>
            </Space>
          </Form.Item>
        </Col>
      </Row>
    </Form>
   
    </Modal>
    </>
  );
}
