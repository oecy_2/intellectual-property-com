import { InboxOutlined } from "@ant-design/icons";
import { Button, Dropdown, Space } from "antd";
import Dragger from "antd/lib/upload/Dragger";
import React, { useState } from "react";

import Increase from './increase';
/**
 * 详细信息设置
 * 批量导入和添加人才信息
 */
export default () => {
  const [showIncrease, setShowIncrease] = useState(false);
  const menu = (
    <div
      style={{
        background: 'rgb(251,251,251)',
        padding: '12px',
        boxShadow: '3px 3px 3px 3px rgb(193,193,195)',

        borderRadius: '3px',
      }}
    >
      <Button
        onClick={() => { }}
        style={{
          overflow: 'hidden',
          whiteSpace: 'normal',
          wordBreak: 'break-all',
          fontSize: '0.9rem',
          padding: '0',
          width: '10vw',
          height: 'auto',
          marginBottom: '2vh',
        }}
      >
        下载导入模板
      </Button>
      <Dragger accept={'.xlsx'} style={{ padding: '0 1vw' }}>
        <p className='ant-upload-drag-icon'>
          <InboxOutlined />
        </p>
        <p className='ant-upload-text'>请拖拽.XLSX文件至此或使用点击上传</p>
        <p className='ant-upload-hint'>
          支持单次或批量上传。 严禁上传公司资料或其他公司文件
        </p>
      </Dragger>
    </div>
  );
  return (
    <>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <h2 style={{ fontWeight: 'bold', display: 'inline-block' }}>
          详细信息
        </h2>
        {
          window.localStorage.getItem('identity') === null ?
            null
            :
            <Space>
              <Dropdown
                trigger={['click']}
                placement='bottomRight'
                arrow
                overlay={menu}
              >
                <Button type='primary'>批量导入</Button>
              </Dropdown>
              <Button type='primary' onClick={() => { setShowIncrease(true) }}>
                添加企业信息
              </Button>
            </Space>
        }
      </div>
      <Increase
        showIncrease={showIncrease}
        setShowIncrease={setShowIncrease}
      />
    </>
  );
}
