declare namespace PT {

  interface Company {
    id: string;
    uuid: string;
    name: string;
    medicalField: string;
    address: string;
    createTime: string;
    mainBusiness: string;
  }
}